package
{
	import flash.display.MovieClip;

	public class ActionManager
	{
		private static var mcGameContainer:MovieClip;
		
		private static var strCurrentAction:String;
		private static var strCurrentTarget:String
		private static var iCurrentTargetIndex:int;
		
		public static function getLastIndex():int{
			// return the index of the last action target
			// useful for performing updates after confirmation menus
			return iCurrentTargetIndex;
		}
		
		public static function initGameContainer(pMc:MovieClip){
			mcGameContainer = pMc;
		}
		
		public static function launchNewAction(pType:int,pTarget:String,pTargetIndex:int,pIndex:int){
			trace("launch action on",pTarget,"pType",pType,"pTargetIndex",pTargetIndex,"pIndex",pIndex);
			
			var strTempType:String;
			
			// record target
			strCurrentTarget = pTarget;
			
			// record index
			iCurrentTargetIndex = pTargetIndex;
			
			/*
			Types:
			0: building
			1: wagon
			2: city
			*/
			
			// find the name of the action based on the passed type
			switch(pType){
				case 0:
					strTempType = CstCities[pTarget].bldType;
					strCurrentAction = CstBuildingDef[strTempType].actions[pIndex];
					break;
				case 1:
					strTempType = CstWagons[pTarget].wagonType;
					strCurrentAction = CstWagons[strTempType].actions[pIndex];
					break;
			}
			
			trace("action is",strCurrentAction);
			
			processAction();
		}
		
		private static function processAction(){
			var arActionArray:Array; // used if an action requires storing an array
			
			trace("processAction:",strCurrentAction);
			
			switch(strCurrentAction){
				case "perform":
					mcGameContainer.showPerformScreen(strCurrentTarget);
					break;
				case "recruit":
					arActionArray = BuildingData.generateRecruits(strCurrentTarget);
					mcGameContainer.showRecruitScreen(strCurrentTarget);
					break;
				case "staff":
					mcGameContainer.showStaffScreen();
					break;
				case "unlock":
					mcGameContainer.showUnlockPopup(iCurrentTargetIndex);
					break;
			}
		}
	}
}