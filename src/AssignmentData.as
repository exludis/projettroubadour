package
{
	public class AssignmentData
	{
		private static var arAssignments:Array;
		private static var arAssignmentsCompleted:Array;
		
		public static function advanceOneDay(){
			// each assignment loses one day.
			// assignments down to zero days left are moved from assignments to assignments completed
			
		}
		
		public static function addArtistToAssignment(pArtist:int,pBuilding:String,pAction:String,pSlot:int){
			//trace("addArtistToAssignment. artist",pArtist,"pBuilding",pBuilding,"pAction",pAction,pSlot);
			
			// check if assignment already exist
			var iTempIndex:int = getAssignmentIndex(pBuilding);
			
			// If it does, add artist to it and save this data
			if(iTempIndex != -1){
				addArtistToAssignmentIndex(pArtist,iTempIndex,pSlot);
				commitAssignments();
				return;
			}
			
			// it doesn't, create a new one
			createNewAssignment(pArtist,pBuilding,pAction,pSlot);
		}
		
		public static function freeSlot(pBuilding:String,pAction:String,pSlot:int){
			//trace("freeSlot");
			
			// check if assignment already exist
			var iTempIndex:int = getAssignmentIndex(pBuilding);
			var bEmpty:Boolean = true;
			
			// If it does, remove what's in thiss lot
			if(iTempIndex != -1){
				arAssignments[iTempIndex].arParticipants[pSlot] = -1; // turn this value to -1 because ints don't take 'null'
				
				// if the assignment is now empty, delete it
				for(var i:int = 0 ; i < arAssignments[iTempIndex].arParticipants.length; i++){
					if(arAssignments[iTempIndex].arParticipants[i] != -1 && arAssignments[iTempIndex].arParticipants[i] != undefined){
						bEmpty = false;
						break;
					}
				}
				if(bEmpty == true){
					arAssignments.splice(iTempIndex,1);
				}
			}
			
			// commit assignments
			commitAssignments();
		}
		
		public static function getArtistsOnAssignment():Array{
			var arToReturn:Array = new Array();
			
			// add all serials found in all assignments
			for each(var o:Object in arAssignments){
				for each(var i:int in o.arParticipants){
					
					// skip if entry was previously made empty
					if(i != -1){
						arToReturn.push(i);
					}
					
				}
			}
			return arToReturn;
		}
		
		public static function getNumParticipantsAt(pBuilding:String):int{
			// this function returns the number of artists planned to take part in a perfromance at the given location
			//trace("getNumParticipantsAt",pBuilding);
			
			var iTempIndex:int = getAssignmentIndex(pBuilding);
			//trace("iTempIndex",iTempIndex);
			
			// if this aassignment doesn't exist yet, return 0
			if(iTempIndex == -1){
				return 0;
			}
			
			var arTempParticipants = arAssignments[iTempIndex].arParticipants;
			var iTempParticipants:int = 0;
			for(var i:int = 0; i < arTempParticipants.length; i++){
				if(arTempParticipants[i] != -1 && arTempParticipants[i] != undefined)iTempParticipants++;
			}
			
			return iTempParticipants;
		}
		
		public static function getParticipantsAt(pBuilding:String):Array{
			// this function returns the array of participants at a given building
			var iTempIndex:int = getAssignmentIndex(pBuilding);
			
			if(iTempIndex != -1){
				return arAssignments[iTempIndex].arParticipants;
			}
			
			return [];
		}
		
		public static function getPrice(pBuilding:String,pAction:String):int{
			//trace("getPrice at",pBuilding);
			
			// note: pAction is not used for the moment, but could be in the future
			
			/*
			For now, this function simply returns the base price for the building., multiplied by the number of performers
			In the future, this value must include these modifiers:
			- Overall reputation of caravan
			- Reputation of caravan in this city
			- Reputation of caravan in this building
			- Reputation of artists
			- Wether or not the line-up looks like the owner's prefered line-up
			  (if he likes a fgull stage, he'll offer more money per performer)
			*/
			
			var iTempCrewSize:int = getNumParticipantsAt(pBuilding); // get the total number of performers
			var iBasePrice:int = CstCities[pBuilding].basePay; // get the base price
			
			return iTempCrewSize * iBasePrice; // return base price * crew size
		}
		
		public static function init(){
			arAssignments = new Array();
		}
		
		public static function retrieveSavedData(){
			arAssignments = SaveManager.getAssignments();
		}
		
		private static function addArtistToAssignmentIndex(pArtist:int,pIndex:int,pSlot:int){
			arAssignments[pIndex].arParticipants[pSlot] = pArtist;
		}
		
		private static function commitAssignments(){
			SaveManager.saveAssignments(arAssignments);
		}
		
		private static function createNewAssignment(pArtist:int,pBuilding:String,pAction:String,pSlot:int){
			//trace("createNewAssignment. artist",pArtist,"pBuilding",pBuilding,"pAction",pAction);
		
			// create an array that can hold the max number of artists in this building
			var iMaxArtists:int = CstCities[pBuilding].maxArtists;
			var arTempParticipants = new Array();
			arTempParticipants[pSlot] = pArtist;
			var tempAssignment:Object = {arParticipants:arTempParticipants,strBuilding:pBuilding,strAction:pAction};
			arAssignments.push(tempAssignment);
			
			commitAssignments();
		}
		
		private static function getAssignmentIndex(pBuilding:String){
			// check all assignments, return index if the building is found
			for(var i:int = 0; i < arAssignments.length; i++){
				if(arAssignments[i].strBuilding == pBuilding){
					return i;
				}
			}
			
			// nothing found, return -1
			return -1;
		}
	}
}