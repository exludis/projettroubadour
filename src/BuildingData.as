package
{
	public class BuildingData
	{
		private static var arBuildingData:Array;
		
		public static function generateRecruits(pBuilding:String):Array{
			var iTempBldIndex:int = checkBuilding(pBuilding);
			
			// determine how much time since last visit
			var iDaysSince:int = getDaysSinceLastVisitAtIndex(iTempBldIndex);
			var iChurnEvents:int;
			var iChurnAmount:int;
			var i:int;
			var j:int;
			
			// If 1st visit, spawn new batch of recruits
			var iNumRecruitsToSpawn:int;
			
			// NEW BUILDING
			if(iDaysSince == -1){
				iNumRecruitsToSpawn = Utils.getRandomIntBetween(CstCities[pBuilding].minRecruits,CstCities[pBuilding].maxRecruits);
				trace("new building visit, spawn",iNumRecruitsToSpawn,"recruits");
				
				// spawn the desired number of recruits for this building
				for(i = 0; i < iNumRecruitsToSpawn; i++){
					spawnNewRecruitForBuilding(pBuilding);
				}
				
				
			// NOT A NEW BUILDING
			// determine number of churn events
			}else{
				trace("new visit to old building");
				// determine number of churn events
				iChurnEvents = Math.floor(iDaysSince / CstCities[pBuilding].churchTime);
				
				trace("recruits will be renewed",iChurnEvents,"times");
				
				// for each churn, lose some recruits, generate other recruits
				for(i = 0; i < iChurnEvents; i++){
					// make some room
					iChurnAmount = Utils.getRandomIntBetween(CstCities[pBuilding].churnMin,CstCities[pBuilding].churnMax);
					deleteRecruitsFromBuilding(pBuilding,iChurnAmount);
					
					// get a random number of new recruits
					iChurnAmount = Utils.getRandomIntBetween(CstCities[pBuilding].churnMin,CstCities[pBuilding].churnMax);
					
					// adjust this number to fit within the buildin's min and max
					iChurnAmount = Utils.setMinMax(iChurnAmount,CstCities[pBuilding].minRecruits,CstCities[pBuilding].maxRecruits);
					
					// create recruits
					for(j = 0; j < iChurnAmount; j++){
						spawnNewRecruitForBuilding(pBuilding);
					}
				}
				
			}
			
			// record time of last visit
			recordVisitAtIndex(iTempBldIndex);
			
			// save buildings
			commitBuildings();
			
			return arBuildingData[iTempBldIndex].currentRecruits;
		}
		
		public static function getPatrons(pBuilding:String):String{
			// find index of this building
			var iTempBldIndex:int = checkBuilding(pBuilding);
			
			// if this building already has patrons for today, return them
			if(getDaysSinceLastVisitAtIndex(iTempBldIndex) == 0 && arBuildingData[iTempBldIndex].patrons != ""){
				return arBuildingData[iTempBldIndex].patrons;
			}
			
			// no patrons for today yet, generate some
			generatePatrons(iTempBldIndex);
			
			return arBuildingData[iTempBldIndex].patrons;
		}
		
		public static function getRecruits(pBuilding:String):Array{
			var iTempBldIndex:int = checkBuilding(pBuilding);
			return arBuildingData[iTempBldIndex].currentRecruits;
		}
		
		public static function init(){
			arBuildingData = new Array();
			
			// commit building data, even though it's empty
			commitBuildings();
		}
		
		public static function removeRecruit(pTarget:String,pSerial:int){
			var iObjIndex:int = checkBuilding(pTarget);
			var tempObj:Object = arBuildingData[iObjIndex];
			var recruitIndex:int = tempObj.currentRecruits.indexOf(pSerial);
			
			if(recruitIndex != -1)tempObj.currentRecruits.splice(recruitIndex,1);
			
			// save buildings
			commitBuildings();
		}
		
		public static function retrieveSavedData(){
			arBuildingData = SaveManager.getBuildings();
		}
		
		private static function checkBuilding(pBuilding:String):int{
			// check if this building exists in the list of saved buildings
			var iTempIndex:int = arBuildingData.indexOf(pBuilding);
			
			// if it exists, return the index of the object
			if(iTempIndex != -1){
				return iTempIndex + 1;
			}
			
			// if not create it
			arBuildingData.push(pBuilding);
			var objTempBuilding:Object = {
				currentRecruits:[],
				currentReputation:0,
				lastVisit:-1, // -1 is a sentinel value for "was never here before"
				patrons:""
			}
				
			arBuildingData.push(objTempBuilding);	
			return arBuildingData.length - 1;
		}
		
		private static function commitBuildings(){
			// this function commits the buildings to the locally saved data
			SaveManager.saveBuildings(arBuildingData);
		}
		
		private static function deleteRecruitsFromBuilding(pBuilding:String,pAmount:int){
			// find the object containing the building data
			var objTempBuilding:Object = arBuildingData[checkBuilding(pBuilding)];
			
			// delete on recruit at random until down to zero, or amount reached
			var iRecruitsDeleted:int = 0;
			var iRecruitIndex:int;
			var iRecruitSerial:int;
			while(objTempBuilding.currentRecruits.length > 0 && iRecruitsDeleted < pAmount){
				iRecruitIndex = Utils.pickAtRandom(objTempBuilding.currentRecruits);
				iRecruitSerial = objTempBuilding.currentRecruits[iRecruitIndex]; // record serial number of recruit
				objTempBuilding.currentRecruits.splice(iRecruitIndex,1);
				CharacterData.deleteRecruitBySerial(iRecruitSerial); // delete this recruit. It is no longer needed
			}
			
		}
		
		private static function generatePatrons(pIndex:int){
			// find name of building
			var strTempBld:String = arBuildingData[pIndex - 1];
			
			// find array of potential patrons
			var arTempPatrons:Array = CstCities[strTempBld].patrons;
				
			// pick one random patron from the list
			var iTempIndex:int = Utils.pickAtRandom(arTempPatrons);
			var strTempPatrons:String = arTempPatrons[iTempIndex];
			
			// assign these patrons to the building
			arBuildingData[pIndex].patrons = strTempPatrons;
		}
		
		private static function getDaysSinceLastVisitAtIndex(pIndex:int):int{
			// if building was never visited, return -1
			if(arBuildingData[pIndex].lastVisit == -1){
				return -1;
			}
			
			return CaravanData.getCurrentDay() - arBuildingData[pIndex].lastVisit;
		}
		
		private static function recordVisitAtIndex(pIndex:int){
			arBuildingData[pIndex].lastVisit = CaravanData.getCurrentDay();
		}
		
		private static function spawnNewRecruitForBuilding(pBuilding:String){
			// spawn a recruit, get its index in return
			var strBuildingType:String = CstCities[pBuilding].bldType;
			var strRecipe:String = CstBuildingDef[strBuildingType].recruitRecipe;
			var iRecruitIndex:int = CharacterData.getNewRecruitIndex(strRecipe);
			
			// add this index to the list of recruites for this building
			var iTempIndex:int = checkBuilding(pBuilding);
			arBuildingData[iTempIndex].currentRecruits.push(iRecruitIndex);
			
		}
	}
}