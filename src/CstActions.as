package
{
	public class CstActions
	{
		
		public static const buyWagon:Object = {
			nameTag:"ACTION_BUYWAGON"
		}
			
		public static const finances:Object = {
			nameTag:"ACTION_FINANCES"
		}
		
		public static const occupants:Object = {
			nameTag:"ACTION_OCCUPANTS"
		}
		
		public static const perform:Object = {
			nameTag:"ACTION_PERFORM"
		}
			
		public static const recruit:Object = {
			nameTag:"ACTION_RECRUIT"
		}
			
		public static const something:Object = {
			nameTag:"ACTION_SOMETHING"
		}
			
		public static const staff:Object = {
			nameTag:"ACTION_STAFF"
		}
			
		public static const unlock:Object = {
			nameTag:"ACTION_UNLOCK"
		}
	}
}