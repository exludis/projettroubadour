package
{
	public class CstBuildingDef
	{
		/*public function CstBuildingDef()
		{
		}*/
		
		public static const typeChurch:Object = {
			typeTag:"BLD_TYPE_CHURCH",
			descrTag:"BLD_DESCR_CHURCH",
			actions:["perform","recruit"],
			recruitRecipe:"churchDefault"
		};
			
		public static const typeTavern:Object = {
			typeTag:"BLD_TYPE_TAVERN",
			descrTag:"BLD_DESCR_TAVERN",
			actions:["perform","recruit","something"],
			recruitRecipe:"tavernDefault"
		};
	}
}