package
{
	public class CstCities
	{
		/*public function CstCities()
		{
		}*/
		
		public static const testVillage:Object = {
			asset:McCityTest,
			buildingTags:[
				"mcTavern","testVillageTavern",
				"mcChurch","testVillageChurch"
			]
		}
			
		public static const testVillageTavern:Object = {
			nameTag:"BLD_TAVERN_TEST",
			bldType:"typeTavern",
			minRecruits:35,
			maxRecruits:37,
			churchTime:7,
			churnMin:1,
			churnMax:2,
			maxArtists:3,
			basePay:50,
			ownerName:"Bob Tavern Owner",
			ownerLikes:["humorous","saucy"],
			ownerDislikes:["pious","patriotic"],
			patrons:["tavernGoers","mercenaries"]
		}
			
		public static const testVillageChurch:Object = {
			nameTag:"BLD_CHURCH_TEST",
			bldType:"typeChurch",
			minRecruits:1,
			maxRecruits:3,
			churchTime:7,
			churnMin:1,
			churnMax:3,
			maxArtists:2,
			basePay:25,
			ownerName:"Jim Church Guy",
			ownerLikes:["pious","epic"],
			ownerDislikes:["saucy"],
			patrons:["churchGoers","pilgrims"]
		}
	}
}