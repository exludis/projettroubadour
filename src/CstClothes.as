package
{
	public class CstClothes
	{
		public static const dressLinenBrown:Object = {
			body:McLinenDressBody,
			upperArm:McLinenDressUpperArm,
			lowerArm:McLinenDressLowerArm,
			colorRecipe:"linenBrown"
		};
		
		public static const dressVelvetBlue:Object = {
			body:McLinenDressBody,
			upperArm:McLinenDressUpperArm,
			lowerArm:McLinenDressLowerArm,
			colorRecipe:"velvetBlue"
		};
		
		public static const shirtLinenGray:Object = {
			body:McLinenDressBody,
			upperArm:McLinenDressUpperArm,
			lowerArm:McLinenDressLowerArm,
			colorRecipe:"linenGray"
		};
		
		public static const shirtVelvetGreen:Object = {
			body:McLinenDressBody,
			upperArm:McLinenDressUpperArm,
			lowerArm:McLinenDressLowerArm,
			colorRecipe:"velvetGreen"
		};
		
		
		
		// color recipes are here
		public static const linenBrown:Object = {
			hue:0,
			saturation:20,
			brightness:-30,
			contrast:0
		};
		
		public static const linenGray:Object = {
			hue:0,
			saturation:0,
			brightness:0,
			contrast:0
		};
		
		public static const velvetBlue:Object = {
			hue:160,
			saturation:70,
			brightness:-40,
			contrast:0
		};
		
		public static const velvetGreen:Object = {
			hue:90,
			saturation:50,
			brightness:-10,
			contrast:0
		};
	}
}