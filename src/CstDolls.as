package
{
	public class CstDolls
	{
		public static const iEyeHeight:int = 15;
		public static const numEyeWidth:Number = 7.0; // distance between eye and middel of face
		public static const numEyeHeightVarriance:int = 2.0; // number of pixels above or below base line eyes can be at.
		public static const numEyeWidthVarriance:Number = 1.8; // nnumber of pixels eyes can move inwards or outwards
		
		public static const numMinBrowVar:Number = -1.1;
		public static const numMaxBrowVar:Number = 2.1;
		
		public static const numNoseHeight:int = 7.0;
		public static const numNoseVarriance:int = 1.6;
		
		public static const iMouthHeight:int = 2;
		public static const iMouthVarriance:int = 2;
		
		public static const numMouthWidth:int = 10;
		public static const iMouthWidthVarriance:int = 3;
		
		public static const arMaleBodyTypes:Array = ["maleThin","maleMedium"];
		public static const arFemaleBodyTypes:Array = ["femaleThin","femaleMedium"];
		
		public static const arMaleStartingClothes:Array = ["shirtLinenGray","shirtVelvetGreen"];
		public static const arFemaleStartingClothes:Array = ["dressLinenBrown","dressVelvetBlue"];
		
		private static const maleThin:Object = {
			body:McMaleBodyThin,
			neck:McNeckThin,
			upperArm:McUpperArmThin,
			lowerArm:McLowerArmThin
		}
			
		private static const maleMedium:Object = {
			body:McMaleBodyMedium,
			neck:McNeckMedium,
			upperArm:McUpperArmThin,
			lowerArm:McLowerArmThin
		}
			
		private static const femaleThin:Object = {
			body:McFemaleBodyThin,
			neck:McNeckThin,
			upperArm:McUpperArmThin,
			lowerArm:McLowerArmThin
		}
		
		private static const femaleMedium:Object = {
			body:McFemaleBodyMedium,
			neck:McNeckMedium,
			upperArm:McUpperArmThin,
			lowerArm:McLowerArmThin
		}
		
		public static const maleHeads:Array = [McMaleHeadOval,McMaleHeadLong,McMaleHeadSquare];
		public static const femaleHeads:Array = [McFemaleHeadOval,McFemaleHeadLong,McFemaleHeadPointy];
		
		public static const arMaleEyeModels:Array = [McEyeRound1,McEyeHalfClosed1,McEyeSlit1];
		public static const arFemaleEyeModels:Array = [McEyeRound1,McEyeHalfClosed1,McEyeLady1];
		
		public static const arMaleNoseModels:Array = [McNoseStraight1,McNoseRound1,McNoseGreek1,McNoseFighter1];
		public static const arFemaleNoseModels:Array = [McNoseStraight1,McNoseRound1,McNoseGreek1,McNoseButton1];
		
		public static const arMaleMouthModels:Array = [McMouthStraight1,McMouthOpen1];
		public static const arFemaleMouthModels:Array = [McMouthStraight1,McMouthOpen1,McMouthThick1];
		
		public static const arMaleBrowModels:Array = [McBrowFull1,McBrowElegant1];
		public static const arFemaleBrowModels:Array = [McBrowElegant1,McBrowLady1];
		
		public static const arMaleHairModels:Array = [McHairPage1,McHairBeatles1];
		public static const arFemaleHairModels:Array = [McHairBraidCrown1,McHairBangs1];
		
		// Skin colours **********************************************************************
		
		public static const arSkinColors:Array = ["skinColourPink","skinColourPale","skinColourOlive"];
		public static const skinColourPink:Object = {
			hue:0,
			saturation:0,
			brightness:0,
			contrast:0
		}
		
		public static const skinColourPale:Object = {
			hue:0,
			saturation:-30,
			brightness:10,
			contrast:20
		}
		
		public static const skinColourOlive:Object = {
			hue:0,
			saturation:-30,
			brightness:-30,
			contrast:10
		}
		
		// ********************************************************************** Skin colours
		
		// Hair colours **********************************************************************
		
		public static const arHairColors:Array = ["hairColourBlond","hairColourDarkBrown","hairColourRed","hairColourLightBrown"];
		public static const hairColourBlond:Object = {
			hue:21,
			saturation:100,
			brightness:80,
			contrast:10
		}
			
		public static const hairColourRed:Object = {
			hue:10,
			saturation:170,
			brightness:40,
			contrast:40
		}
			
		public static const hairColourDarkBrown:Object = {
			hue:0,
			saturation:-10,
			brightness:-20,
			contrast:10
		}
			
		public static const hairColourLightBrown:Object = {
			hue:0,
			saturation:0,
			brightness:0,
			contrast:0
		}
		
		
		// ********************************************************************** Hair colours
		
		public static function getBody(pIsFemale:Boolean,pType:int):Class{
			var strBodyRecipe:String;
			if(pIsFemale == true){
				strBodyRecipe = arFemaleBodyTypes[pType];
			}else{
				strBodyRecipe = arMaleBodyTypes[pType];
			}
			
			return CstDolls[strBodyRecipe].body;
		}
		
		public static function getBrowModel(pIsFemale:Boolean,pType:int):Class{
			if(pIsFemale == true){
				return arFemaleBrowModels[pType];
			}
			
			return arMaleBrowModels[pType];
		}
		
		public static function getEyeModel(pIsFemale:Boolean,pType:int):Class{
			if(pIsFemale == true){
				return arFemaleEyeModels[pType];
			}
			
			return arMaleEyeModels[pType];
		}
		
		public static function getHairColorRecipe(pType:int):String{
			return arHairColors[pType];
		}
		
		public static function getHairModel(pIsFemale:Boolean,pType:int):Class{
			if(pIsFemale == true){
				return arFemaleHairModels[pType];
			}
			
			return arMaleHairModels[pType];
		}
		
		public static function getHead(pIsFemale:Boolean,pType:int):Class{
			if(pIsFemale == true){
				return femaleHeads[pType];
			}
			
			return maleHeads[pType];
		}
		
		public static function getLowerArm(pIsFemale:Boolean,pType:int):Class{
			var strBodyRecipe:String;
			if(pIsFemale == true){
				strBodyRecipe = arFemaleBodyTypes[pType];
			}else{
				strBodyRecipe = arMaleBodyTypes[pType];
			}
			
			return CstDolls[strBodyRecipe].lowerArm;
		}
		
		public static function getMouthModel(pIsFemale:Boolean,pType:int):Class{
			if(pIsFemale == true){
				return arFemaleMouthModels[pType];
			}
			
			return arMaleMouthModels[pType];
		}
		
		public static function getNeck(pIsFemale:Boolean,pType:int):Class{
			var strBodyRecipe:String;
			if(pIsFemale == true){
				strBodyRecipe = arFemaleBodyTypes[pType];
			}else{
				strBodyRecipe = arMaleBodyTypes[pType];
			}
			
			return CstDolls[strBodyRecipe].neck;
		}
		
		public static function getNoseModel(pIsFemale:Boolean,pType:int):Class{
			if(pIsFemale == true){
				return arFemaleNoseModels[pType];
			}
			
			return arMaleNoseModels[pType];
		}
		
		public static function getRandomBodyType(bIsFemale:Boolean):int{
			if(bIsFemale == true){
				return Math.random() * arFemaleBodyTypes.length;
			}
			
			return Math.random() * arMaleBodyTypes.length;
		}
		
		public static function getRandomBrowType(bIsFemale:Boolean):int{
			if(bIsFemale == true){
				return Math.random() * arFemaleBrowModels.length;
			}
			
			return Math.random() * arMaleBrowModels.length;
		}
		
		public static function getRandomBrowVar():Number{
			return Math.random() * (numMaxBrowVar - numMinBrowVar) + numMinBrowVar;
		}
		
		public static function getRandomClothes(bIsFemale:Boolean):String{
			var iTempIndex:int;
			if(bIsFemale == true){
				iTempIndex = Math.random() * arFemaleStartingClothes.length;
				return arFemaleStartingClothes[iTempIndex];
			}
			
			iTempIndex = Math.random() * arMaleStartingClothes.length;
			return arMaleStartingClothes[iTempIndex];
		}
		
		public static function getRandomEyeType(bIsFemale:Boolean):int{
			if(bIsFemale == true){
				return Math.random() * arFemaleEyeModels.length;
			}
			
			return Math.random() * arMaleEyeModels.length;
		}
		
		public static function getRandomHairColor():int{
			return Math.random() * arHairColors.length;
		}
		
		public static function getRandomHairType(bIsFemale:Boolean):int{
			if(bIsFemale == true){
				return Math.random() * arFemaleHairModels.length;
			}
			
			return Math.random() * arMaleHairModels.length;
		}
		
		public static function getRandomHeadType(bIsFemale:Boolean):int{
			if(bIsFemale == true){
				return Math.random() * femaleHeads.length;
			}
			
			return Math.random() * maleHeads.length;
		}
		
		public static function getRandomMouthType(bIsFemale:Boolean):int{
			if(bIsFemale == true){
				return Math.random() * arFemaleMouthModels.length;
			}
			
			return Math.random() * arMaleMouthModels.length;
		}
		
		public static function getRandomNoseType(bIsFemale:Boolean):int{
			if(bIsFemale == true){
				return Math.random() * arFemaleNoseModels.length;
			}
			
			return Math.random() * arMaleNoseModels.length;
		}
		
		public static function getRandomSkinColor():int{
			return Math.random() * arSkinColors.length;
		}
		
		public static function getSkinColorRecipe(pType:int):String{
			return arSkinColors[pType];
		}
		
		public static function getUpperArm(pIsFemale:Boolean,pType:int):Class{
			var strBodyRecipe:String;
			if(pIsFemale == true){
				strBodyRecipe = arFemaleBodyTypes[pType];
			}else{
				strBodyRecipe = arMaleBodyTypes[pType];
			}
			
			return CstDolls[strBodyRecipe].upperArm;
		}
	}
}