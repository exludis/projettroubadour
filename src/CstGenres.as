package
{
	public class CstGenres
	{
			
		public static const children:Object = {
			nameTag:"GENRE_CHILDREN"
		};
		
		public static const epic:Object = {
			nameTag:"GENRE_EPIC"
		};
		
		public static const gallant:Object = {
			nameTag:"GENRE_GALLANT"
		};
		
		public static const humorous:Object = {
			nameTag:"GENRE_HUMOROUS"
		};
		
		public static const military:Object = {
			nameTag:"GENRE_MILITARY"
		};
		
		public static const patriotic:Object = {
			nameTag:"GENRE_PATRIOTIC"
		};
		
		public static const pious:Object = {
			nameTag:"GENRE_PIOUS"
		};
			
		public static const saucy:Object = {
			nameTag:"GENRE_SAUCY"
		};
			
	}
}