package
{
	public class CstNames
	{
		public static const franc:Object = {
			firstNamesM:[
				"Sigimond","Luc","Thibauld","Enselme",
				"Martin","Romuald","Pierre","Calixte"
				],
			firstNamesF:[
				"Cunegonde","Mathilde","Aure","Margaux",
				"Flore","Gertrude","Berthe","Flore"
				],
			lastNames:[
				"Boulanger","Marchand","Vallette","Saint-Louis"
				]
		};
		
		public static const angle:Object = {
			
		};
		
		public static const teuton:Object = {
			
		};
	}
}