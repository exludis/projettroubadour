package
{
	public class CstPatrons
	{
		public static const churchGoers:Object = {
			nameTag:"PATRON_CHURCHGOERS",
			likes:["pious","epic"],
			dislikes:[]
		}
		
		public static const mercenaries:Object = {
			nameTag:"PATRON_MERCENARIES",
			likes:["humorous","saucy","epic"],
			dislikes:["pious"]
		}
		
		public static const pilgrims:Object = {
			nameTag:"PATRON_PILGRIMS",
			likes:["pious"],
			dislikes:[]
		}
		
		public static const tavernGoers:Object = {
			nameTag:"PATRON_TAVERNGOERS",
			likes:["humorous","saucy"],
			dislikes:["pious"]
		}
	}
}