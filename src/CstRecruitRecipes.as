package
{
	public class CstRecruitRecipes
	{
		public static const churchDefault:Object = {
			mainSkill:["singing","playing"],
			mainSkillAvg:55,
			mainSkillMax:80,
			mainSkillMin:40,
			othSkillAvg:30,
			othSkillMin:0,
			othSkillMax:60,
			attitudeAvg:60,
			attitudeMin:30,
			attitudeMax:100,
			fightingAvg:20,
			fightingMin:0,
			fightingMax:70,
			minGenres:0,
			maxGenres:2,
			genres:["pious","epic","patriotic"]
		}
			
		public static const tavernDefault:Object = {
			mainSkill:["singing","playing","dancing"],
			mainSkillAvg:55,
			mainSkillMax:80,
			mainSkillMin:40,
			othSkillAvg:30,
			othSkillMin:0,
			othSkillMax:60,
			attitudeAvg:60,
			attitudeMin:30,
			attitudeMax:100,
			fightingAvg:20,
			fightingMin:0,
			fightingMax:70,
			minGenres:0,
			maxGenres:2,
			genres:["saucy","epic","humorous"]
		}
	}
}