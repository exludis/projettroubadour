package
{
	public class CstStrings
	{
		
		private static const strBadString:String = "!!!TAG";
		private static const strBadLang:String = "!!!LANG";
		
		private static const arLanguages:Array = ["english","français"];
		private static const arLanguageTags:Array = ["eng","fra"];
		private static var strLanguage:String; // used for fetching language name from an int
		
		// **************************************************************************************
		// **************************************************************************************
		// **************************************************************************************
		// Strings start here, in alphabetical order
		// **************************************************************************************
		
		// ***************************
		// General strings
		
		private static const GEN_AND:Object = {eng:"and",fra:"et"};
		private static const GEN_ATTITUDE:Object = {eng:"Attitude",fra:"Personnalité"};
		private static const GEN_CHARISMA:Object = {eng:"charisma",fra:"charisme"};
		private static const GEN_BACKCITY:Object = {eng:"Back to city",fra:"À la ville"};
		private static const GEN_CONSTITUTION:Object = {eng:"constitution",fra:"constitution"};
		private static const GEN_CONTRACT:Object = {eng:"contract",fra:"contrat"};
		private static const GEN_CURRENTSTATUS:Object = {eng:"current status",fra:"état actuel"};
		private static const GEN_DANCERM:Object = {eng:"dancer",fra:"danceur"};
		private static const GEN_DANCERF:Object = {eng:"dancer",fra:"danceuse"};
		private static const GEN_DANCING:Object = {eng:"dancing",fra:"danse"};
		private static const GEN_DONE:Object = {eng:"Done",fra:"Confirmer"};
		private static const GEN_DAYSLEFT:Object = {eng:"days left",fra:"jours"};
		private static const GEN_ENERGY:Object = {eng:"energy",fra:"énergie"};
		private static const GEN_FIGHTING:Object = {eng:"fighting",fra:"combat"};
		private static const GEN_GENRES:Object = {eng:"genres",fra:"styles"};
		private static const GEN_GREED:Object = {eng:"greed",fra:"avarice"};
		private static const GEN_HAPPINESS:Object = {eng:"happiness",fra:"bonheur"};
		private static const GEN_HEALTH:Object = {eng:"health",fra:"santé"};
		private static const GEN_INSTRUMENT:Object = {eng:"instrument",fra:"instrument"};
		private static const GEN_MOOD:Object = {eng:"mood",fra:"humeur"};
		private static const GEN_NO:Object = {eng:"no",fra:"non"};
		private static const GEN_PLAYERM:Object = {eng:"player",fra:"joueur"};
		private static const GEN_PLAYERF:Object = {eng:"player",fra:"joueuse"};
		private static const GEN_RELIABILITY:Object = {eng:"reliability",fra:"fiabilité"};
		private static const GEN_SINGERM:Object = {eng:"singer",fra:"chanteur"};
		private static const GEN_SINGERF:Object = {eng:"singer",fra:"chanteuse"};
		private static const GEN_SINGING:Object = {eng:"singing",fra:"chant"};
		private static const GEN_SKILL:Object = {eng:"Skill",fra:"Habileté"};
		private static const GEN_YES:Object = {eng:"yes",fra:"oui"};
		
		// General strings
		// ***************************
		
		// ***************************
		// Action strings
		
		private static const ACTION_BUYWAGON:Object = {eng:"Buy wagon",fra:"Acheter voiture"};
		private static const ACTION_CHANGEEQUIP:Object = {eng:"Equipment",fra:"Équipement"};
		private static const ACTION_CHANGELODGING:Object = {eng:"Change lodging",fra:"Déménager"};
		private static const ACTION_DISLIKED:Object = {eng:"Disikes",fra:"N'aime pas"};
		private static const ACTION_FINANCES:Object = {eng:"Finances",fra:"Finances"};
		private static const ACTION_FIRE:Object = {eng:"Fire",fra:"Renvoyer"};
		private static const ACTION_FIREDESCR:Object = {
			eng:"Do you want to fire $NAME? You will not be refunded for the time left on $HISHER contract.",
			fra:"Désirez-vous renvoyer $NAME? Vous ne recevrez aucun remboursement pour les jours restants à son contrat."};
		private static const ACTION_HIRE:Object = {eng:"hire",fra:"engager"};
		private static const ACTION_LIKED:Object = {eng:"Likes",fra:"Aime"};
		private static const ACTION_NOSTAFF:Object = {eng:"Empty troup",fra:"Troupe vide"};
		private static const ACTION_OCCUPANTS:Object = {eng:"Occupants",fra:"Résidents"};
		private static const ACTION_OWNER:Object = {eng:"Owner",fra:"Patron"};
		private static const ACTION_PATRONS:Object = {eng:"Patrons",fra:"Public"};
		private static const ACTION_PAY:Object = {eng:"Pay",fra:"Paye"};
		private static const ACTION_PICK:Object = {eng:"Pick",fra:"Choisir"};
		private static const ACTION_RECRUIT:Object = {eng:"Recruit",fra:"Recruter"};
		private static const ACTION_RECRUIT_NOFUNDS = {eng:"Not enough money",fra:"Manque d'argent"};
		private static const ACTION_RECRUIT_NONE:Object = {
			eng:"No recruit to show",
			fra:"Aucun candidat"
		};
		private static const ACTION_RECRUIT_NOPLACE = {eng:"Not enough room",fra:"Manque d'espace"};
		private static const ACTION_RENEW:Object = {eng:"Renew",fra:"Ré-embaucher"};
		private static const ACTION_PERFORM:Object = {eng:"Perform",fra:"Jouer"};
		private static const ACTION_PERFORMINFO:Object = {eng:"View info",fra:"Voir informations"};
		private static const ACTION_SELECTPERFORMERS:Object = {eng:"Select performers",fra:"Choisir artistes"};
		private static const ACTION_SOMETHING:Object = {eng:"Something",fra:"Quelque chose"};
		private static const ACTION_STAFF:Object = {eng:"Staff",fra:"Personnel"};
		private static const ACTION_UNLOCK:Object = {eng:"Unlock",fra:"Débloquer"};
		private static const ACTION_UNLOCK_DESCR:Object = {
			eng:"Do you want to unlock this spot for for $INT?",
			fra:"Désirez-vous débloquer cet espace pour $INT?"
		};
		
		// Action strings
		// ***************************
		
		
		// ***************************
		// Building strings
		
		private static const BLD_CHURCH_TEST:Object = {eng:"Test Church",fra:"Église Test"};
		
		private static const BLD_DESCR_CHURCH:Object = {
			eng:"Churches are great venues for acts that are clean and wholesome.",
			fra:"Les églises sont un site merveilleux pour les artistes au style propre et sain."
		};
		private static const BLD_DESCR_TAVERN:Object = {
			eng:"Taverns are venues for all styles, depending on the patrons. Different publics will expect different genres.",
			fra:"Les tavernes sont des salles de spectacle au style qui change à chaque jour, en fonction du public présent."
		};
		
		private static const BLD_TAVERN_TEST:Object = {eng:"Test Tavern",fra:"Tarverne Test"};
		
		private static const BLD_TYPE_CHURCH:Object = {eng:"Church",fra:"Église"};
		private static const BLD_TYPE_TAVERN:Object = {eng:"Tavern",fra:"Taverne"};
		// Building strings
		// ***************************
		
		
		// ***************************
		// Genres strings
		
		private static const GENRE_CHILDREN:Object = {eng:"Children",fra:"Pour enfants"};
		private static const GENRE_EPIC:Object = {eng:"Epic",fra:"Épique"};
		private static const GENRE_GALLANT:Object = {eng:"Gallant",fra:"Gallant"};
		private static const GENRE_HUMOROUS:Object = {eng:"Humorous",fra:"Humoristique"};
		private static const GENRE_MILITARY:Object = {eng:"Military",fra:"Militaire"};
		private static const GENRE_PATRIOTIC:Object = {eng:"Patriotic",fra:"Patriotique"};
		private static const GENRE_PIOUS:Object = {eng:"Pious",fra:"Pieux"};
		private static const GENRE_SAUCY:Object = {eng:"Saucy",fra:"Grivois"};
		
		
		// Genres strings
		// ***************************
		
		
		// ***************************
		// HUD strings
		
		private static const HUD_NEXTDAY:Object = {eng:"Next Day",fra:"Jour Suivant"};
		
		// HUD strings
		// ***************************
		
		
		// ***************************
		// Menu strings
		
		private static const MENU_CONTINUE:Object = {eng:"Continue",fra:"Continuer"};
		private static const MENU_CREDITS:Object = {eng:"Credits",fra:"Générique"};
		private static const MENU_ENTERNAME:Object = {eng:"Enter name here",fra:"Entrer le nom ici"};
		private static const MENU_LANGUAGE:Object = {eng:"Language",fra:"Langue"};
		private static const MENU_LANGUAGE_PROMPT:Object = {
			eng:"Select a language",
			fra:"Choisissez une langue."
		};
		private static const MENU_NEWGAME:Object = {eng:"New Game",fra:"Nouvelle Partie"};
		private static const MENU_NEWGAMEEXPL:Object = {
			eng:"Before you start, you need to name your troup of travelling performers.",
			fra:"Avant de débuter, il faut nommer votre troupe d'artistes ambulants."
		};
		private static const MENU_PLAYBTN:Object = {eng:"Go!",fra:"Go!"};
		private static const MENU_RESTART:Object = {eng:"Restart",fra:"Recommencer"};
		private static const MENU_TUTPROMPT:Object = {
			eng:"Do you want to play the tutorial?",
			fra:"Voulez-vous les instructions?"
		};
		
		// Menu strings
		// ***************************
		
		
		// ***************************
		// Patrons strings
		
		private static const PATRON_CHURCHGOERS:Object = {eng:"Church goers",fra:"Paroissiens"};
		private static const PATRON_MERCENARIES:Object = {eng:"Mercenaries",fra:"Mercenaires"};
		private static const PATRON_PILGRIMS:Object = {eng:"Pilgrims",fra:"Pélerins"};
		private static const PATRON_TAVERNGOERS:Object = {eng:"Tavern goers",fra:"Clientèle de taverne"};
		
		
		// Patrons strings
		// ***************************
		
		
		
		// ***************************
		// Wagon strings
		
		private static const WAGON_CAMPFIRE:Object = {eng:"Camp fire",fra:"Bivouac"};
		private static const WAGON_CAMPFIRE_DESCR:Object = {
			eng:"People sleep in the open.",
			fra:"Les gens dorment à la belle étoile."
		};
		private static const WAGON_EMPTYSPOT:Object = {eng:"Empty spot",fra:"Espace libre"};
		private static const WAGON_EMPTYSPOT_DESCR:Object = {
			eng:"You could place a part of your caravan here ... for the right price.",
			fra:"Vous pourriez installer une partie de votre caravane ici ... si vous y mettiez le prix."
		};
		private static const WAGON_GENERIC:Object = {eng:"Wagon",fra:"Voiture"};
		private static const WAGON_OFFICE:Object = {eng:"Office",fra:"Bureau"};
		private static const WAGON_OFFICE_DESCR:Object = {
			eng:"This is where the caravan is run.",
			fra:"C'est ici que les décisions sont prises."
		};
		
		// Wagon strings
		// ***************************
		
		
		
		// **************************************************************************************
		// Strings end here, in alphabetical order
		// **************************************************************************************
		// **************************************************************************************
		// **************************************************************************************
		
		
		
		public static function getLanguage(pIndex:int):String{
			return arLanguages[pIndex];
		}
		
		public static function getString(pTag:String):String{
			// if unable to find the object, return a message
			if(!CstStrings[pTag]){
				return strBadString;
			}
			
			strLanguage = arLanguageTags[Settings.iCurentLanguage];
			
			// return specified language string
			return CstStrings[pTag][strLanguage];
		}
	}
}