package
{
	public class CstWagons
	{
		public static const campFire:Object = {
			asset:McWagonCampFire,
			wagonType:"typeCampfire",
			nameTag:"WAGON_CAMPFIRE",
			beds:6
		}
			
		public static const emptySpot:Object = {
			asset:McCaravanSpot,
			wagonType:"typeEmpty",
			nameTag:"WAGON_EMPTYSPOT",
			beds:0
		}
		
		public static const office:Object = {
			asset:McWagonOffice,
			wagonType:"typeOffice",
			nameTag:"WAGON_OFFICE",
			beds:0
		}
		
		// ***************************
		// TYPES *********************
			
		public static const typeCampfire:Object = {
			actions:["buyWagon","occupants"],
			descrTag:"WAGON_CAMPFIRE_DESCR"
		}
			
		public static const typeEmpty:Object = {
			actions:["unlock"],
			descrTag:"WAGON_EMPTYSPOT_DESCR"
		}
			
		public static const typeOffice:Object = {
			actions:["staff","finances"],
			descrTag:"WAGON_OFFICE_DESCR"
		}
	}
}