package 
{
	import flash.display.BitmapData;
	import flash.display.MovieClip;

	public class DollMaker
	{
		
		private static var arIcons:Array;
		private static var arPictures:Array;
		
		public static function getIcon(pSerial:int):BitmapData{
			// if the array of icons doesn't reach the needed serial, fill it with null
			makeListsReach(pSerial);
			
			// if there is already an icon in the specified index, return it
			if(arIcons[pSerial] != null){
				return arIcons[pSerial];
			}
			
			// generate icon and return the results
			generateAssets(pSerial);
			return arIcons[pSerial];
		}
		
		public static function getPicture(pSerial:int):BitmapData{
			// make sure the serial is positive because, in certain cases, a negative serial is used to differentiate artists
			pSerial = Math.abs(pSerial);
			
			// if the array of icons doesn't reach the needed serial, fill it with null
			makeListsReach(pSerial);
			
			// if there is already an icon in the specified index, return it
			if(arPictures[pSerial] != null){
				return arPictures[pSerial];
			}
			
			// generate icon and return the results
			generateAssets(pSerial);
			return arPictures[pSerial];
		}
		
		public static function init(){
			arIcons = new Array();
			arPictures = new Array();
		}
		
		/*public static function getUpdatedIcon(pSerial:int):BitmapData{
			var bmpBitmap:BitmapData;
			
			return bmpBitmap;
		}*/
		
		/*public static function getUpdatedPicture(pSerial:int):BitmapData{
			var bmpBitmap:BitmapData;
			
			return bmpBitmap;
		}*/
		
		private static function generateAssets(pSerial:int){
			// generate the dress-up doll
			var mcTempDoll:MovieClip = new McDressUpDoll(pSerial);
			
			// take a picture of the dress-up doll and put it in the array
			arPictures[pSerial] = mcTempDoll.getPicture();
			arIcons[pSerial] = mcTempDoll.getIcon();
		}
		
		private static function makeListsReach(pSerial:int){
			// This function makes sure the icons and pictures arrays reache the needed serial
			
			// nothing to do if the array has the needed length
			if(arIcons.lenght >= pSerial + 1){
				return;
			}
			
			// add as many null as possible
			while(arIcons.lenght < pSerial + 1){
				arIcons.push(null);
				arPictures.push(null);
			}
		}
		
	}
}