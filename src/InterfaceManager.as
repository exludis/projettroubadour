package
{
	import flash.display.MovieClip;

	public class InterfaceManager
	{
		private static var mcHud:MovieClip;
		
		public static function initHud(pMc:MovieClip){
			mcHud = pMc;
		}
		
		public static function updateHud(){
			mcHud.updateStats();
		}
	}
}