package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class McActionScreenPrototype extends McPrototype
	{
		private var mcBackDrop:MovieClip;
		protected var strTarget:String; // used to record the asset this screen was launched for
		protected var mcCloseButton:MovieClip;
		protected var mcProtoBackground:MovieClip; // holds a reference to the background.
		
		public function McActionScreenPrototype(pParent:MovieClip)
		{
			super(pParent);
			
		}
		
		protected override function addedToStageFunction(){
			// center self on stage using the background
			// because with hidden components, this.height and this.width could be out of whack
			this.x = Math.floor(stage.stageWidth / 2 - mcProtoBackground.width / 2);
			this.y = Math.floor(stage.stageHeight / 2 - mcProtoBackground.height / 2);
			
			// build backdrop
			buildBackDrop();
			
			// listen to close button
			mcCloseButton.addEventListener("buttonClicked",closeHandler);
			
			// start a dummy function because at least one child class uses it
			addedToStageFunction2();
		}
		
		protected function addedToStageFunction2(){
			// this is a dummy function. It's here for children classes to add code at the end of addedToStageFunction();
		}
		
		protected function buildBackDrop(){
			mcBackDrop = new McBackdrop();
			mcBackDrop.x = -this.x;
			mcBackDrop.y = -this.y;
			addChildAt(mcBackDrop,0);
		}
		
		private function closeHandler(e:Event){
			//trace("closeHandler");
			mcCloseButton.removeEventListener("buttonClicked",closeHandler);
			dispatchEvent(new Event("closeClicked"));
		}
	}
}