package
{
	import flash.display.MovieClip;
	
	public class McAssetConfPopup extends McConfPopupProto
	{
		private var mcAsset:MovieClip;
		
		public function McAssetConfPopup(pParent:MovieClip)
		{
			super(pParent);
			mcYesButtonProto = mcYesButton;
			mcNoButtonProto = mcNoButton;
			tfConfText = tfConfTextHere;
			
		}
		
		public function updateGraphic(pType:int,pClass:Class){
			// This function replaces the placeholder with the passet graphic
			
			/*
			Types:
			0: building
			1: wagon
			2: city
			*/
			
			mcAsset = new pClass();
			
			// place asset but pivot point varries from class to class
			switch(pType){
				case 1:
					// Wagon: center asset origin on placeholder center point
					mcAsset.x = mcPlaceholder.x + mcPlaceholder.width / 2;
					mcAsset.y = mcPlaceholder.y - mcPlaceholder.height / 2;
					break;
			}
			
			// remove placeholder and add new asset
			removeChild(mcPlaceholder);
			addChild(mcAsset);
			
			
		}
	}
}