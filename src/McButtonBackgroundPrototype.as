package  
{
	import flash.display.MovieClip;

	public class McButtonBackgroundPrototype extends MovieClip
	{
		public function McButtonBackgroundPrototype()
		{
			//trace("button background prototype constructor");
			gotoUp();
		}
		
		public function gotoClicked(){
			gotoAndStop("clicked");
		}
		
		public function gotoDisabled(){
			gotoAndStop("disabled");
		}
		
		public function gotoDown(){
			gotoAndStop("down");
		}
		
		public function gotoUp(){
			gotoAndStop("out");
		}
		
		public function gotoOver(){
			gotoAndStop("over");
		}
	}
}