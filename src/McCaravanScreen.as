package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.ui.Mouse;
	
	public class McCaravanScreen extends McGameScreenPrototype
	{
		private var arSpots:Array;
		
		public function McCaravanScreen(pParent:MovieClip)
		{
			super(pParent);
			
			// build array of everty spot
			arSpots = [mcSpot1,mcSpot2,mcSpot3,mcSpot4,mcSpot5,mcSpot6,mcSpot7,mcSpot8,mcSpot9,mcSpot10,mcSpot11];
			
			// one by one, replace spots with wagons if the caravan has one
			for(var i :int = 0 ; i < arSpots.length; i++){
				updateSpot(i);
			}
			
			// update the city button
			mcCityButton.setText(CstStrings.getString("GEN_BACKCITY"));
			
		}
		
		public override function actionButton(){
			// store index of action button that was clicked
			var iActionIndex = mcContextMenu.iActionButtonIndex;
			
			// find the index of the source object
			var iTargetIndex:int = arSpots.indexOf(mcContextMenuSource);
			
			// flush the context menu
			flushContextMenu();
			
			// Transmit action to the action manager class
			ActionManager.launchNewAction(iContextMenuType,strContextMenuTarget,iTargetIndex,iActionIndex);
		}
		
		public function updateSpot(pIndex:int){
			var strTempContent:String;
			var tempClass:Class;
			var tempMc:MovieClip;
			
			// get the name of the wagon here
			strTempContent = CaravanData.getContentInSpot(pIndex);
			
			// If there is more than an empty spot, add the necessary asset
			if(strTempContent != "emptySpot"){
				tempClass = CstWagons[strTempContent].asset;
				tempMc = new tempClass();
				arSpots[pIndex].addChild(tempMc);
			}
		}
		
		protected override function addedToStageFunction(){
			startSpotsListeners();
			// start city button listener
			mcCityButton.addEventListener("buttonClicked",cityButtonHandler);
		}
		
		protected override function removedFromStageFunction(){
			stopSpotsListeners();
			// stop city button listener
			mcCityButton.removeEventListener("buttonClicked",cityButtonHandler);
		}
		
		private function cityButtonHandler(e:Event){
			mcMyParent.startCityScreen();
		}
		
		private function spotClickedHandler(e:MouseEvent){
			trace("spot clicked");
			var mcTempTarget:MovieClip = e.currentTarget as MovieClip;
			var iIndexOfWagon:int = arSpots.indexOf(mcTempTarget);
			var strWagon:String = CaravanData.getWagonNameAtIndex(iIndexOfWagon);
			//trace("strWagon",strWagon);
			showContextMenu(this,1,mcTempTarget,strWagon,iIndexOfWagon);
		}
		
		private function startSpotsListeners(){
			for(var i :int = 0 ; i < arSpots.length; i++){
				arSpots[i].addEventListener(MouseEvent.CLICK,spotClickedHandler);
			}
		}
		
		private function stopSpotsListeners(){
			for(var i :int = 0 ; i < arSpots.length; i++){
				arSpots[i].removeEventListener(MouseEvent.CLICK,spotClickedHandler);
			}
		}
		
	}
}