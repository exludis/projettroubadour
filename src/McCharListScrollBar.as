package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class McCharListScrollBar extends McPrototype
	{
		private var iScrollBarMinY:int; // store the min Y coordinate of the scroll bar
		private var iScrollBarMaxY:int; // store the max Y coordinate of the scroll bar
		
		private var iSliderMouseY:int; // records the Y coordinate of mouse contact with slider
		private var numSliderRatio:Number; // ratio of slider on its track
		
		public function McCharListScrollBar()
		{
			var mcTempMc:MovieClip = new MovieClip(); // create dummy parent
			super(mcTempMc);
			
			// remove click timer on slider and make it reset automatically
			mcSlider.removeClickTimer();
			mcSlider.setAutoReset(true);
		}
		
		public function adjustSliderSize(pContainerSize:int,pWindowSize:int){
			//trace("adjustSliderSize");
			
			// figure out min position of slider
			iScrollBarMinY = mcButtonUp.y + mcButtonUp.height;
			
			// the slider should hide its track in a ratio that's the inverse of the ratio between content and window
			var iTrackLength:int = mcButtonDown.y - mcButtonDown.height - iScrollBarMinY;			
			var numShowRatio:Number = pWindowSize / pContainerSize;
			//var numTrackCoverRatio:Number = 1 - numShowRatio;
			var iSliderLength:int = Math.round(numShowRatio * iTrackLength);
			
			mcSlider.mcBackground.height = iSliderLength;
			
			// determine its lowest possible position
			iScrollBarMaxY = iScrollBarMinY + iTrackLength - mcSlider.height;
		}
		
		public function getRatio():Number{
			return numSliderRatio;
		}
		
		public function placeSlider(pMin:int,pCurrent:int,pMax:int){
			// figure out the ratio on the track
			numSliderRatio = 1 - (pCurrent - pMin) / (pMax - pMin);
			
			// place track according to this
			mcSlider.y = Math.round(iScrollBarMinY + (iScrollBarMaxY - iScrollBarMinY) * numSliderRatio);
		}
		
		protected override function addedToStageFunction(){
			// add event listeners to arrows
			mcButtonUp.addEventListener("buttonClicked",arrowUpHandler);
			mcButtonDown.addEventListener("buttonClicked",arrowDownHandler);
			mcSlider.addEventListener("buttonDown",sliderDownHandler);
		}
		
		protected override function removedFromStageFunction(){
			mcButtonUp.removeEventListener("buttonClicked",arrowUpHandler);
			mcButtonDown.removeEventListener("buttonClicked",arrowDownHandler);
			mcSlider.removeEventListener("buttonDown",sliderDownHandler);
		}
		
		private function arrowDownHandler(e:Event){
			dispatchEvent(new Event("arrowDown"));
			
			// reset button
			mcButtonDown.enable();
		}
		
		private function arrowUpHandler(e:Event){
			dispatchEvent(new Event("arrowUp"));
			
			// reset button
			mcButtonUp.enable();
		}
		
		private function sliderDownHandler(e:Event){
			// record contact point between slider and pointer
			iSliderMouseY = mouseY - mcSlider.y;
			
			// listen to mouse movements instead of the slider
			mcSlider.removeEventListener("buttonDown",sliderDownHandler);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, sliderMoveHandler);
			stage.addEventListener(MouseEvent.MOUSE_UP, sliderReleaseHandler); // added to stage, in case you bring it up anywhere
			
		}
		
		private function sliderMoveHandler(e:MouseEvent){
			// line up slider with mouse along Y axis
			mcSlider.y = Utils.setMinMax((mouseY - iSliderMouseY),iScrollBarMinY,iScrollBarMaxY);
			
			// record ration along the slider track
			numSliderRatio = (mcSlider.y - iScrollBarMinY) / (iScrollBarMaxY - iScrollBarMinY);
			
			// dispatch event to let parent mc know the slider has moved
			dispatchEvent(new Event("sliderDrag"));
		}
		
		private function sliderReleaseHandler(e:MouseEvent){
			// mouse up, reset listeners
			mcSlider.addEventListener("buttonDown",sliderDownHandler);
			stage.removeEventListener(MouseEvent.MOUSE_MOVE, sliderMoveHandler);
			stage.removeEventListener(MouseEvent.MOUSE_UP, sliderReleaseHandler);
		}
	}
}