package
{
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	
	public class McCharacterCard extends McButtonPrototype
	{
		private var iCharacterSerial:int; // records the serial number of this character
		
		public function McCharacterCard(pParent:MovieClip)
		{
			super();
			
			// set mouse children to false
			mouseChildren = false;
			
			// set button background
			mcButtonBackground = mcBackground;
			
			// rename the character and skill fields
			tfSkill.text = CstStrings.getString("GEN_SKILL");
			tfCharacter.text = CstStrings.getString("GEN_ATTITUDE");
		}
		
		public function getSerial():int{
			return iCharacterSerial;
		}
		
		public function showCharacter(pSerial:int){
			// Show icon where the placeholder is and remove it
			var bmIcon:Bitmap = new Bitmap();
			bmIcon.bitmapData = DollMaker.getIcon(pSerial);
			bmIcon.x = mcIconPlaceholder.x;
			bmIcon.y = mcIconPlaceholder.y;
			removeChild(mcIconPlaceholder);
			mcIconPlaceholder = null;
			addChild(bmIcon);
			
			// record serial
			iCharacterSerial = pSerial;
			
			var tempObject:Object = CharacterData.getCharacter(pSerial);
			
			// write down name
			tfName.text = CharacterData.getCharacterName(tempObject);
			
			// write down role
			tfFunction.text = CharacterData.getCharacterRole(tempObject);
			
			// write down ratings
			var iRatingOn10:int = CharacterData.getSkillRating(tempObject);
			mcSkillRating.gotoAndStop(iRatingOn10 + 1);
			iRatingOn10 = CharacterData.getAttitudeRating(tempObject);
			mcAttitudeRating.gotoAndStop(iRatingOn10 + 1);
		}
	}
}