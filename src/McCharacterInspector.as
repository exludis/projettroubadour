package
{
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	
	public class McCharacterInspector extends McPrototype
	{
		private var objCurrentChar:Object; // records the character being inspected
		private var iCurrentSerial:int; // records the serial of the character currently inspected
		private var bmPicture:Bitmap;
		private var iPictureX:int;
		private var iPictureY:int;
		
		public function McCharacterInspector()
		{
			var mcDummy:MovieClip = new MovieClip(); // dummy mc to pass as parameter
			super(mcDummy);
			
			// record picture coordinates and remove placholder
			iPictureX = mcPicPlaceholder.x;
			iPictureY = mcPicPlaceholder.y;
			removeChild(mcPicPlaceholder);
			mcPicPlaceholder = null;
			
			// update all legend fields
			tfInstrument.text = CstStrings.getString("GEN_INSTRUMENT");
			tfSinging.text = CstStrings.getString("GEN_SINGING");
			tfDancing.text = CstStrings.getString("GEN_DANCING");
			tfCharisma.text = CstStrings.getString("GEN_CHARISMA");
			tfMood.text = CstStrings.getString("GEN_MOOD");
			tfReliability.text = CstStrings.getString("GEN_RELIABILITY");
			tfConstitution.text = CstStrings.getString("GEN_CONSTITUTION");
			tfFighting.text = CstStrings.getString("GEN_FIGHTING");
			tfGreed.text = CstStrings.getString("GEN_GREED");
			tfGenres.text = CstStrings.getString("GEN_GENRES");
			tfCurrentStatus.text = CstStrings.getString("GEN_CURRENTSTATUS");
			tfHappiness.text = CstStrings.getString("GEN_HAPPINESS");
			tfHealth.text = CstStrings.getString("GEN_HEALTH");
			tfEnergy.text = CstStrings.getString("GEN_ENERGY");
			tfCost.text = CstStrings.getString("GEN_CONTRACT");
			tfDays.text = CstStrings.getString("GEN_DAYSLEFT");
			
			updateEmpty();
		}
		
		public function inspectSerial(pSerial:int){
			//trace("inspect character with serial",pSerial);
			
			// place graphic on screen
			bmPicture = new Bitmap();
			bmPicture.bitmapData = DollMaker.getPicture(pSerial);
			bmPicture.x = iPictureX;
			bmPicture.y = iPictureY;
			addChild(bmPicture);
			
			// record serial in case it may be needed later
			iCurrentSerial = pSerial;
			
			// fetch object for this character
			objCurrentChar = CharacterData.getCharacter(pSerial);
			
			// update name and role fields
			tfName.text = CharacterData.getCharacterName(objCurrentChar);
			tfRole.text = CharacterData.getCharacterRole(objCurrentChar);
			
			// update playing stat
			tfPlayingStat.text = objCurrentChar.playing;
			
			// update other stats
			tfSingingStat.text = objCurrentChar.singing;
			tfDancingStat.text = objCurrentChar.dancing;
			tfCharismaStat.text = objCurrentChar.charisma;
			tfMoodStat.text = objCurrentChar.mood;
			tfConstitutionStat.text = objCurrentChar.constitution;
			tfFightingStat.text = objCurrentChar.fighting;
			tfReliabilityStat.text = objCurrentChar.reliability;
			tfGreedStat.text = objCurrentChar.greed;
			tfHappinessStat.text = objCurrentChar.happiness;
			tfHealthStat.text = objCurrentChar.health;
			tfEnergyStat.text = objCurrentChar.energy;
			tfCostStat.text = objCurrentChar.cost;
			
			// update genres
			tfGenre1Stat.text = CharacterData.getGenreName(pSerial,1);
			tfGenre2Stat.text = CharacterData.getGenreName(pSerial,2);
			tfGenre3Stat.text = CharacterData.getGenreName(pSerial,3);
			
			// if contractDays == -1, it means no contract
			if(objCurrentChar.contractDays == -1){
				tfDaysStat.text = "--";
			}else{
				tfDaysStat.text = String(objCurrentChar.contractDays);
			}
		}
		
		public function update(){
			inspectSerial(iCurrentSerial);
		}
		
		public function makeEmpty(){
			objCurrentChar = null;
			updateEmpty();
		}
		
		private function updateEmpty(){
			// remove graphic
			if(bmPicture){
				removeChild(bmPicture);
				bmPicture = null;
			}
			
			
			// replace all fields by "--"
			tfName.text = "--";
			tfRole.text = "--";
			tfPlayingStat.text = "--";
			tfSingingStat.text = "--";
			tfDancingStat.text = "--";
			tfCharismaStat.text = "--";
			tfMoodStat.text = "--";
			tfConstitutionStat.text = "--";
			tfFightingStat.text = "--";
			tfReliabilityStat.text = "--";
			tfGreedStat.text = "--";
			tfHappinessStat.text = "--";
			tfHealthStat.text = "--";
			tfEnergyStat.text = "--";
			tfCostStat.text = "--";
			tfDaysStat.text = "--";
		}
	}
}