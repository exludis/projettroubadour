package
{
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;

	
	public class McCharacterList extends McPrototype
	{
		private var arCards:Array;
		private var mcListContainer:MovieClip; // holds the scrolling elements
		private var maskerShape:Shape; // shape that will mask the container
		
		private var iCurrentCharSerial:int; // serial of currently clicked character
		
		private var iBottomExtent:int; // records the last pixel of the container
		
		// values to record min and max coordinates for container
		private var iContainerMinY:int;
		private var iContainerMaxY:int;
		
		private var iArrowMoveDistance:int = 60; // by how many pixels does the list move when clicking an arrow
		
		public function McCharacterList()
		{
			var mcFakeMc:MovieClip = new MovieClip(); // build a fake mc to pass as parent
			super(mcFakeMc);
			
			// create list container and apply a mask
			maskerShape = new Shape();
			maskerShape.graphics.beginFill(0x0);
			maskerShape.graphics.drawRect(2, 2, 238, 276);
			maskerShape.graphics.endFill();
			addChild(maskerShape);
			
			mcListContainer = new MovieClip();
			mcListContainer.mask = maskerShape;
			addChild(mcListContainer);
			
			// remove arrow from slider on scroll bar
			mcScrollBar.mcSlider.removeChild(mcScrollBar.mcSlider.mcArrow);
			
			// set iCurrentCharSerial to -1, meaning no card is selected
			iCurrentCharSerial = -1;
			
			// by default, start scroll bar listeners
			startScrollBarListeners();
		}
		
		public function buildCharList(pList:Array){
			// prepare array that contains cards
			arCards = new Array();
			var mcTempCard:MovieClip;
			
			// reset bottom extent
			iBottomExtent = 0;
			
			// for each element in the list, create a character card
			for(var i:int = 0 ; i < pList.length; i++){
				mcTempCard = new McCharacterCard(this);
				mcTempCard.showCharacter(pList[i]);
				mcTempCard.x = 2;
				mcTempCard.y = 2 + 80 * i;
				mcListContainer.addChild(mcTempCard);
				arCards.push(mcTempCard);
				// update the bottom extent of the container
				iBottomExtent = mcTempCard.y + mcTempCard.height;
			}
			
			// if the character list is shorter than the window, remove the scroll bar
			updateScrollBar();
			
			// update empty message
			updateEmptyMessage();
		}
		
		public function disableCardWithSerial(pSerial:int){
			// go through all cards. Disable and stop function when the one with the passed serial is found
			for each(var mc:MovieClip in arCards){
				if(mc.getSerial() == pSerial){
					mc.disable();
					return;
				}
			}
		}
		
		public function disableOnAssignment(){
			//trace("disableOnAssignment");
			
			// get all artists on assignement
			var arArtistsOnAssignment:Array = AssignmentData.getArtistsOnAssignment();
			//trace("arArtistsOnAssignment",arArtistsOnAssignment);
			
			for each(var mc:MovieClip in arCards){
				
				// this artist is not on assignment
				if(arArtistsOnAssignment.indexOf(mc.getSerial()) == -1){
					mc.enable();
				
				// this artist is on assignment
				}else{
					mc.disable();
				}
			}
		}
		
		public function forceListenersOn(){
			// force button-style listeners on cards
			for(var i:int = 0 ; i < arCards.length; i++){
				arCards[i].startButtonListeners();
			}
			
			startCardListeners();
		}
		
		public function getCurrentSerial():int{
			return iCurrentCharSerial;
		}
		
		public function removeCharSerial(pSerial:int){
			// this is probably the currently selected serial. if so, change it to -1
			if(iCurrentCharSerial == pSerial)iCurrentCharSerial = -1;
			
			var iCardIndex:int = -1;
			// go through cards, looking for serial
			for(var i:int = 0; i < arCards.length; i++){
				
				// if the card has been found, move other cards up by one spot
				if(iCardIndex != -1){
					arCards[i].y -= arCards[i].height;
					// update the bottom extent of the container
					iBottomExtent = arCards[i].y + arCards[i].height;
				}
				
				// if this is the card, write down its index and remove it
				if(arCards[i].getSerial() == pSerial){
					mcListContainer.removeChild(arCards[i]);
					iCardIndex = i;
					// update the bottom extent of the container
					iBottomExtent -= arCards[i].height;
				}
				
			}
			
			// remove found card from the list of cards
			if(iCardIndex != -1)arCards.splice(iCardIndex,1);
			
			// update the scroll bar
			var bScrollBar:Boolean = updateScrollBar();
			
			// if there is no scroll bar, make sure to set the list back to starting position
			if(bScrollBar == false){
				mcListContainer.y = 0;
			
			}
			
			// update empty message
			updateEmptyMessage();
		}
		
		public function setEmptyMessage(pMessage:String){
			tfNoCard.text = pMessage;
		}
		
		protected override function addedToStageFunction(){
			// update the scroll bar
			updateScrollBar();
			
			startCardListeners();
			
			
		}
		
		protected override function removedFromStageFunction(){
			stopCardListeners();
			
			// if the scrollbar is there, remove listeners
			if(mcScrollBar){
				stopScrollBarListeners();
			}
		}
		
		private function cardDownHandler(e:Event){
			// update all cards except the selected one
			for(var i:int = 0 ; i < arCards.length; i++){
				if(arCards[i] != e.currentTarget as MovieClip){
					arCards[i].enable();
				}
			}
			
			// record serial of current caracter being clicked
			iCurrentCharSerial = e.currentTarget.getSerial();
			
			// start listening for mouse up
			e.currentTarget.addEventListener(MouseEvent.MOUSE_UP,cardUpHandler);
			
			// dispatch a card down event
			dispatchEvent(new Event("cardDown"));
			
			// also dispatch a card change event
			dispatchEvent(new Event("cardChange"));
		}
		
		private function cardUpHandler(e:Event){
			// stop listening to mouse up
			e.currentTarget.removeEventListener(MouseEvent.MOUSE_UP,cardUpHandler);
			
			dispatchEvent(new Event("cardUp"));
		}
		
		private function scrollArrowDownHandler(e:Event){
			// calculate next y coordinate
			var iY:int = Math.max(mcListContainer.y - iArrowMoveDistance, iContainerMinY);
			
			mcListContainer.y = iY;
			
			// adjust slider position
			mcScrollBar.placeSlider(iContainerMinY,mcListContainer.y,iContainerMaxY);
		}
		
		private function scrollArrowUpHandler(e:Event){
			// calculate next y coordinate
			var iY:int = Math.min(mcListContainer.y + iArrowMoveDistance, iContainerMaxY);
			
			mcListContainer.y = iY;
			
			// adjust slider position
			mcScrollBar.placeSlider(iContainerMinY,mcListContainer.y,iContainerMaxY);
		}
		
		private function scrollSliderHandler(e:Event){
			// get ratio from scroll bar
			var numScrollRatio:Number = 1 - mcScrollBar.getRatio();
			
			// move container to the same ratio
			mcListContainer.y = Math.round(iContainerMinY + (iContainerMaxY - iContainerMinY) * numScrollRatio);
		}
		
		private function startCardListeners(){
			trace("startCardListeners");
			for(var i:int = 0 ; i < arCards.length; i++){
				// check if listener already exist
				// because some function force start listeners
				// and this may cause the same listeners to be added more than once
				
				if(!arCards[i].hasEventListener("buttonDown")){
					arCards[i].addEventListener("buttonDown",cardDownHandler);
				}
			}
			
		}
		
		private function stopCardListeners(){
			for(var i:int = 0 ; i < arCards.length; i++){
				arCards[i].removeEventListener(MouseEvent.MOUSE_DOWN,cardDownHandler);
			}
		}
		
		private function startScrollBarListeners(){
			mcScrollBar.addEventListener("arrowUp",scrollArrowUpHandler);
			mcScrollBar.addEventListener("arrowDown",scrollArrowDownHandler);
			mcScrollBar.addEventListener("sliderDrag",scrollSliderHandler);
		}
		
		private function stopScrollBarListeners(){
			mcScrollBar.removeEventListener("arrowUp",scrollArrowUpHandler);
			mcScrollBar.removeEventListener("arrowDown",scrollArrowDownHandler);
			mcScrollBar.removeEventListener("sliderDrag",scrollSliderHandler);
		}
		
		private function updateEmptyMessage(){
			// this function shows or hides the empty message, based on whether or not there are cards to display
			
			// 0 cards
			if(arCards.length == 0){
				// message is not on stage
				if(this.getChildByName("tfNoCard") == null){
					addChild(tfNoCard);
				}
			
			// at least one card
			}else{
				// message is on stage
				if(this.getChildByName("tfNoCard") != null){
					removeChild(tfNoCard);
				}
			}
		}
		
		private function updateScrollBar():Boolean{
			// function returns true if the scrollbar is on screen, false otherwise
			
			// record if scrollbar actually on-screen
			var bScrollBarOnScreen:Boolean = false;
			if(mcScrollBar)bScrollBarOnScreen = true;
			
			
			// if the character list is shorter than the window, and the scrollbar is on-screen, remove the scroll bar
			if(iBottomExtent < 276 && bScrollBarOnScreen == true){
				// stop listeners
				stopScrollBarListeners();
				
				removeChild(mcScrollBar);
				mcScrollBar = null;
				
				return false;
			}
			
			// if there us no change, stop here
			if(iBottomExtent < 276 && bScrollBarOnScreen == false){
				return false;
			}
			
			// if the scrollbar needs to be spawned, do it
			if(bScrollBarOnScreen == false){
				mcScrollBar = new McCharListScrollBar();
				mcScrollBar.x = this.width;
				mcScrollBar.y = 0;
				
				// remove arrow from slider on scroll bar
				mcScrollBar.mcSlider.removeChild(mcScrollBar.mcSlider.mcArrow);
				
				// start listeners
				startScrollBarListeners();
			}
			
			// otherwise, calculate max and min positions and adjust slider size
			iContainerMinY = -iBottomExtent + maskerShape.height + 2;
			iContainerMaxY = 0;
			mcScrollBar.adjustSliderSize(iBottomExtent,maskerShape.height);
			
			// make sure the container is not above its limit
			if(bScrollBarOnScreen == true)mcListContainer.y = Math.max(iContainerMinY,mcListContainer.y);
			
			// place scrollbar
			mcScrollBar.placeSlider(iContainerMinY,mcListContainer.y,iContainerMaxY);
			
			return true;
		}
	}
}