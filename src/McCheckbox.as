package  
{
	import flash.events.MouseEvent;

	public class McCheckbox extends McButtonPrototype
	{
		public var bIsChecked:Boolean = true;
		
		public function McCheckbox()
		{
			super();
			mcButtonBackground = mcCheckboxBackground;
		}
		
		public function getCheck():Boolean{
			return bIsChecked;
		}
		
		protected override function mouseClickHandler(e:MouseEvent){
			//trace("click");
			mcButtonBackground.gotoClicked();
			toggleCheckmark();
		}
		
		protected function toggleCheckmark(){
			bIsChecked = !bIsChecked;
			updateCheckmark();
		}
		
		protected function updateCheckmark(){
			mcCheckmark.alpha = int(bIsChecked);
		}
	}
}