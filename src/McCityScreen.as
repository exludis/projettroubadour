package
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	public class McCityScreen extends McGameScreenPrototype	
	{
		private var strCurrentCity:String
		private var mcCurrentCity:MovieClip;
		private var arBuildings:Array;
		
		public function McCityScreen(pParent:MovieClip,pCity:String)
		{
			//trace("building city screen for",pCity);
			super(pParent);
			strCurrentCity = pCity;
			var clsCityClass:Class = CstCities[pCity].asset;
			mcCurrentCity = new clsCityClass();
			addChild(mcCurrentCity);
			
			// build list of children buildings
			// Go child by child
			var mcTempChild:MovieClip;
			var strTempName:String;
			arBuildings = new Array();
			for(var i:int = 0; i < mcCurrentCity.numChildren; i++){
				
				// If a child has a name other than "instance"(looking for "ins" in name), add it to the list of interactive objects
				mcTempChild = mcCurrentCity.getChildAt(i) as MovieClip;
				strTempName = mcTempChild.name;
				if(strTempName.substr(0,3) != "ins"){
					arBuildings.push(mcTempChild);
				}
				
			}
			//trace("arBuildings",arBuildings);
			startBuildingListeners();
		}
		
		private function buildingClickHandler(e:MouseEvent){
			//trace("click");
			var strBuildingName:String = e.currentTarget.name;
			
			// if clicking on caravan, go to the caravan screen
			if(strBuildingName == "mcCaravan"){
				mcMyParent.startCaravanScreen();
				return;
			}
			
			var iIndexOfBuilding:int = CstCities[strCurrentCity].buildingTags.indexOf(strBuildingName);
			var strBuildingDef:String = CstCities[strCurrentCity].buildingTags[iIndexOfBuilding+1];
			
			showContextMenu(this,0,(e.currentTarget as MovieClip),strBuildingDef,0);
		}
		
		private function startBuildingListeners(){
			for(var i:int = 0; i < arBuildings.length; i++){
				arBuildings[i].addEventListener(MouseEvent.CLICK,buildingClickHandler);
			}
		}
	}
}