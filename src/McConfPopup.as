package
{
	import flash.display.MovieClip;
	
	public class McConfPopup extends McConfPopupProto
	{
		public function McConfPopup(pParent:MovieClip)
		{
			super(pParent);
			mcYesButtonProto = mcYesButton;
			mcNoButtonProto = mcNoButton;
			tfConfText = tfConfTextHere;
		}
	}
}