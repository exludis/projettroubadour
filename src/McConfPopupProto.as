package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.text.TextField;
	
	public class McConfPopupProto extends McPopupPrototype
	{
		protected var mcYesButtonProto:MovieClip;
		protected var mcNoButtonProto:MovieClip;
		protected var tfConfText:TextField;
		
		public function McConfPopupProto(pParent:MovieClip)
		{
			super(pParent);
		}
		
		public function setText(pText:String){
			tfConfText.text = pText;
		}
		
		protected override function addedToStageFunction2(){
			// update text in yes an no buttons
			mcYesButtonProto.setText(CstStrings.getString("GEN_YES"));
			mcNoButtonProto.setText(CstStrings.getString("GEN_NO"));
			
			// add listeners to both buttons
			mcYesButtonProto.addEventListener("buttonClicked",clickYesHandler);
			mcNoButtonProto.addEventListener("buttonClicked",clickNoHandler);
		}
		
		protected function clickYesHandler(e:Event){
			dispatchEvent(new Event("yesClicked"));
		}
		
		protected function clickNoHandler(e:Event){
			dispatchEvent(new Event("noClicked"));
		}
	}
}