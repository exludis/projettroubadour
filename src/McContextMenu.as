package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class McContextMenu extends McPrototype
	{
		private var mcGraphic:MovieClip;
		
		private var arActionButtons:Array;
		
		private var iActionButtonX:int;
		private var iActionButtonY:int;
		private var iActionButtonHeight:int;
		
		public var iActionButtonIndex:int; // when an aciton button is clicked, this stores its index for later retrieval
		
		private const iButtonBuffer:int = 5; // number of pixels between action buttons
		
		public function McContextMenu(pParent:MovieClip)
		{
			super(pParent);
			
			// delete placeholder action button and store its location and size for future reference.
			iActionButtonX = mcActionButton.x;
			iActionButtonY = mcActionButton.y;
			iActionButtonHeight = mcActionButton.height;
			removeChild(mcActionButton);
		}
		
		public function buildActionButtons(pArray:Array){
			// prepare array that holds the buttons
			arActionButtons = new Array();
			
			// build each button
			var strTempActionTxt:String;
			var mcTempButton:MovieClip;
			for(var i:int = 0; i < pArray.length; i++){
				strTempActionTxt = pArray[i];
				//trace("build button for",strTempActionTxt);
				mcTempButton = new McContextMenuButton(); // create button
				//mcTempButton.setText(strTempActionTxt); // write text in it
				mcTempButton.setText(CstStrings.getString(CstActions[strTempActionTxt].nameTag));
				arActionButtons.push(mcTempButton); // add it to the array of buttons
				
				// place the button
				mcTempButton.x = iActionButtonX;
				mcTempButton.y = iActionButtonY + i * (iActionButtonHeight + iButtonBuffer);
				
				// put button on screen
				addChild(mcTempButton);
			}
			
			// adjust background of the mc to wrap around buttons
			// use last button added as reference
			mcBackground.height = mcTempButton.y + mcTempButton.height;
		}
		
		public function setGraphic(pMc:MovieClip){
			// figure out class of passed mc, in order to create a copy
			var clsGraphic:Class = pMc.constructor;
			//trace("mc constructor",pMc.constructor);
			mcGraphic = new clsGraphic();
			
			// place mc where the bottom middle of the placeholder is
			mcGraphic.x = mcPlaceholder.x + mcPlaceholder.width / 2 - mcGraphic.width / 2;
			mcGraphic.y = mcPlaceholder.y - mcGraphic.height;
			
			//  now add the child and remove the placeholder
			removeChild(mcPlaceholder);
			addChild(mcGraphic);
			
		}
		
		public function setGraphicWagon(pClass:Class){
			// figure out class of passed mc, in order to create a copy
			mcGraphic = new pClass();
			
			// place mc where the middle of the placeholder is
			mcGraphic.x = mcPlaceholder.x + mcPlaceholder.width / 2;
			mcGraphic.y = mcPlaceholder.y - mcPlaceholder.height / 2;
			
			//  now add the child and remove the placeholder
			removeChild(mcPlaceholder);
			addChild(mcGraphic);
			
		}
		
		protected override function addedToStageFunction(){
			startButtonListeners();
		}
		
		protected override function removedFromStageFunction(){
			stopButtonListeners();
			arActionButtons = null;
		}
		
		private function clickActionHandler(e:Event){
			iActionButtonIndex = arActionButtons.indexOf(e.currentTarget as MovieClip);
			mcMyParent.actionButton();
		}
		
		private function clickCloseHandler(e:Event){
			dispatchEvent(new Event("closeClicked"));
		}
		
		private function startButtonListeners(){
			// start close button listener
			mcCloseButton.addEventListener("buttonClicked",clickCloseHandler);
			
			// start action buttons listeners
			for(var i:int = 0 ; i < arActionButtons.length; i++){
				arActionButtons[i].addEventListener("buttonClicked",clickActionHandler);
			}
		}
		
		private function stopButtonListeners(){
			// stop close button listener
			mcCloseButton.removeEventListener("buttonClicked",clickCloseHandler);
			
			// stop action buttons listeners
			for(var i:int = 0 ; i < arActionButtons.length; i++){
				arActionButtons[i].removeEventListener("buttonClicked",clickActionHandler);
			}
		}
	}
}