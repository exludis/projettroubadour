package 
{
	import fl.motion.AdjustColor;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.filters.ColorMatrixFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	
	public class McDressUpDoll extends MovieClip
	{
		// background of image
		private var mcBackground:MovieClip;
		
		// manequin movieclip
		private var mcManequin:MovieClip;
		
		// Frame of the image
		private var rectFrame:Shape;
		
		// Mask of the image
		private var rectMask:Shape;
		
		// *********************************************
		// Doll body nodes, from lowest layer to highest
		
		private var mcHatBack:MovieClip;
		private var mcBody:MovieClip;
		private var mcNeck:MovieClip;
		private var mcShoulderLeft:MovieClip;
		private var mcShoulderRight:MovieClip;
		private var mcUpperArmLeft:MovieClip;
		private var mcUpperArmRight:MovieClip;
		private var mcLowerArmLeft:MovieClip;
		private var mcLowerArmRight:MovieClip;
		private var mcHandLeft:MovieClip;
		private var mcHandRight:MovieClip;
		
		// Doll body nodes, from lowest layer to highest
		// *********************************************
		
		// *********************************************
		// Doll head nodes, from lowest layer to highest
		
		private var mcHead:MovieClip;
		private var mcEyeRight:MovieClip;
		private var mcEyeLeft:MovieClip;
		private var mcNose:MovieClip;
		private var mcMouth:MovieClip;
		private var mcBrowLeft:MovieClip;
		private var mcBrowRight:MovieClip;
		
		private var mcHairFront:MovieClip;
		private var mcHairBack:MovieClip;
		
		// Doll head nodes, from lowest layer to highest
		// *********************************************
		
		// *********************************************
		// Doll clothes, from lowest layer to highest
		private var mcClothesBody:MovieClip;
		private var mcClothesBodyBack:MovieClip;
		private var mcClothesUpperArmLeft:MovieClip;
		private var mcClothesUpperArmRight:MovieClip;
		private var mcClothesLowerArmLeft:MovieClip;
		private var mcClothesLowerArmRight:MovieClip;
		
		// Doll clothes, from lowest layer to highest
		// *********************************************
		
		
		
		// var about the model
		private var bIsFemale:Boolean;
		private var iPosture:int;
		
		
		// var for the mask
		private var maskerShape:Shape;
		
		private const arInstrumentList:Array = [
			"",
			"lute"
			];
		
		public function McDressUpDoll(pSerial:int)
		{
			// find the posture of the manequin
			iPosture = getPostureFor(pSerial);
			
			// find out the gender of the manequin
			bIsFemale = CharacterData.checkFemale(pSerial);
			
			if(bIsFemale == true){
				mcManequin = new McManequinF();
			}else{
				mcManequin = new McManequinM();
			}
			
			//trace("****** building character doll ******");
			//trace("name",CharacterData.getCharacterNamePerSerial(pSerial));
			//trace("female?",bIsFemale);
			
			// place background
			mcBackground = new McPictureBackground();
			addChild(mcBackground);
			
			// prepare body references and values
			var tempClass:Class;
			var iTempBodyType:int = CharacterData.getBodyType(pSerial);
			var iHeadType:int = CharacterData.getHeadType(pSerial);
			var iTempModelType:int;
			var iColorIndex:int;
			var strColorRecipe:String;
			var strClothes:String = CharacterData.getCharacterClothes(pSerial);
			
			// place body
			tempClass = CstDolls.getBody(bIsFemale,iTempBodyType);
			mcBody = new tempClass();
			lineUp(mcBody,mcManequin.mcBodyNode);
			addChild(mcBody);
			
			// place neck
			tempClass = CstDolls.getNeck(bIsFemale,iTempBodyType);
			mcNeck = new tempClass();
			lineUp(mcNeck,mcManequin.mcNeckNode);
			addChild(mcNeck);
			
			// attach clothing parts to body ********************************
			tempClass = CstClothes[strClothes].body;
			mcClothesBody = new tempClass();
			removeClothesAtBack(mcClothesBody);// remove all children that go to the back of the body
			// line up with body. Don't attach to body because later, when a color filter will be applied
			// the filter of the body would interact with the filter of the clothes. We don't want that!
			lineUp(mcClothesBody,mcBody);
			addChild(mcClothesBody);
			// now add clothing behind the body
			mcClothesBodyBack = new tempClass();
			removeClothesAtFront(mcClothesBodyBack);// remove all children that go to the back of the body
			lineUp(mcClothesBodyBack,mcBody);
			addChildAt(mcClothesBodyBack,2);// add at index 2 so that the back hair is behid the back clothing
			// ******************************** attach clothing parts to body
			
			// place head
			tempClass = CstDolls.getHead(bIsFemale,iHeadType);
			mcHead = new tempClass();
			lineUp(mcHead,mcManequin.mcHeadNode);
			addChild(mcHead);
			
			// place arms and hands ***************************
			// upper right arm ... to be rotated 180 degrees.
			tempClass = CstDolls.getUpperArm(bIsFemale,iTempBodyType);
			mcUpperArmRight = new tempClass();
			lineUp(mcUpperArmRight,mcManequin.mcArmUpperRightNode);
			mcUpperArmRight.rotation += 180;
			addChild(mcUpperArmRight);
			
			// upper left arm ... to be flipped and rotated 180 degrees.
			mcUpperArmLeft = new tempClass();
			lineUp(mcUpperArmLeft,mcManequin.mcArmUpperLeftNode);
			mcUpperArmLeft.scaleX = -1;
			mcUpperArmLeft.rotation += 180;
			addChild(mcUpperArmLeft);
			
			// apply clothing to upper arms ********************************
			// upper arm right
			tempClass = CstClothes[strClothes].upperArm;
			mcClothesUpperArmRight = new tempClass();
			lineUp(mcClothesUpperArmRight,mcUpperArmRight);
			addChild(mcClothesUpperArmRight);
			
			// upper arm left
			mcClothesUpperArmLeft = new tempClass();
			mcClothesUpperArmLeft.scaleX = -1;
			lineUp(mcClothesUpperArmLeft,mcUpperArmLeft);
			addChild(mcClothesUpperArmLeft);
			// ******************************** apply clothing to upper arms 
			
			// lower right arm ... to be rotated 180 degrees.
			tempClass = CstDolls.getLowerArm(bIsFemale,iTempBodyType);
			mcLowerArmRight = new tempClass();
			lineUp(mcLowerArmRight,mcManequin.mcArmLowerRightNode);
			mcLowerArmRight.rotation += 180;
			addChild(mcLowerArmRight);
			
			// lower left arm ... to be flipped and rotated 180 degrees.
			mcLowerArmLeft = new tempClass();
			lineUp(mcLowerArmLeft,mcManequin.mcArmLowerLeftNode);
			mcLowerArmLeft.scaleX = -1;
			mcLowerArmLeft.rotation += 180;
			addChild(mcLowerArmLeft);
			
			// apply clothing to lower arms ********************************
			// lower arm right
			tempClass = CstClothes[strClothes].lowerArm;
			mcClothesLowerArmRight = new tempClass();
			lineUp(mcClothesLowerArmRight,mcLowerArmRight);
			addChild(mcClothesLowerArmRight);
			
			// upper arm left
			mcClothesLowerArmLeft = new tempClass();
			mcClothesLowerArmLeft.scaleX = -1;
			lineUp(mcClothesLowerArmLeft,mcLowerArmLeft);
			addChild(mcClothesLowerArmLeft);
			// ******************************** apply clothing to lower arms
			
			// right hand ... rotated 90 degrees counterclockwise
			tempClass = McHandNeutral;
			mcHandRight = new tempClass();
			lineUp(mcHandRight,mcManequin.mcHandRightNode);
			mcHandRight.rotation += 90;
			addChild(mcHandRight);
			
			// left hand ... rotated 90 degrees clockwise and flipped
			tempClass = McHandNeutral;
			mcHandLeft = new tempClass();
			lineUp(mcHandLeft,mcManequin.mcHandLeftNode);
			mcHandLeft.scaleX = -1;
			mcHandLeft.rotation -= 90;
			addChild(mcHandLeft);
			
			// *************************** place arms and hands
			
			// build face ***************************
			
			// place right eye
			iTempModelType = CharacterData.getCharacterEyeType(pSerial);
			tempClass = CstDolls.getEyeModel(bIsFemale,iTempModelType);
			mcEyeRight = new tempClass();
			mcHead.addChild(mcEyeRight);// attach to head
			mcEyeRight.y = -CstDolls.iEyeHeight + CharacterData.getCharacterEyeHeightVar(pSerial); // raise eyes in face until it reaches the specified height
			mcEyeRight.x = CstDolls.numEyeWidth + CharacterData.getCharacterEyeWidthVar(pSerial); // move eyes away from nose
			
			// place left eye
			mcEyeLeft = new tempClass();
			mcHead.addChild(mcEyeLeft);// attach to head
			mcEyeLeft.y = mcEyeRight.y; // raise eyes in face until it reaches the specified height
			mcEyeLeft.x = -mcEyeRight.x;
			mcEyeLeft.scaleX = -1;
			
			//trace("brow model",CharacterData.getCharacterBrowType(pSerial));
			
			// place brow right
			iTempModelType = CharacterData.getCharacterBrowType(pSerial);
			tempClass = CstDolls.getBrowModel(bIsFemale,iTempModelType);
			mcBrowRight = new tempClass();
			mcHead.addChild(mcBrowRight);// attach to head
			mcBrowRight.x = mcEyeRight.x;
			mcBrowRight.y = mcEyeRight.y + CharacterData.getCharacterBrowVar(pSerial);
			
			// place brow left
			mcBrowLeft = new tempClass();
			mcHead.addChild(mcBrowLeft);// attach to head
			mcBrowLeft.x = mcEyeLeft.x;
			mcBrowLeft.y = mcBrowRight.y;
			mcBrowLeft.scaleX = -1;
			
			// place mouth
			iTempModelType = CharacterData.getCharacterMouthType(pSerial);
			tempClass = CstDolls.getMouthModel(bIsFemale,iTempModelType);
			mcMouth = new tempClass();
			mcHead.addChild(mcMouth);// attach to head
			mcMouth.y = -CstDolls.iMouthHeight + CharacterData.getCharacterMouthVar(pSerial);
			mcMouth.width = CharacterData.getCharacterMouthWidth(pSerial);
			
			// place nose
			iTempModelType = CharacterData.getCharacterNoseType(pSerial);
			tempClass = CstDolls.getNoseModel(bIsFemale,iTempModelType);
			mcNose = new tempClass();
			mcHead.addChild(mcNose);// attach to head
			mcNose.y = -CstDolls.numNoseHeight + CharacterData.getCharacterNoseVar(pSerial);
			mcNose.height = -mcEyeLeft.y + mcNose.y + 3; // adjust nose height so that the top arrives slightly above the eyes
			
			// *************************** build face
			
			// change skin color
			iColorIndex = CharacterData.getCharacterSkinColor(pSerial);
			strColorRecipe = CstDolls.getSkinColorRecipe(iColorIndex);
			changeSkinColours(strColorRecipe);
			
			// add Hair ***************************
			//trace("adding front hair");
			// front hair
			iTempModelType = CharacterData.getCharacterHairType(pSerial);
			tempClass = CstDolls.getHairModel(bIsFemale,iTempModelType);
			mcHairFront = new tempClass();
			// remove face model and hair at bottom
			mcHairFront.removeChildAt(0);
			mcHairFront.removeChildAt(0);
			// attach to head
			mcHead.addChild(mcHairFront);
			
			//trace("adding back hair");
			// back hair
			mcHairBack = new tempClass();
			// remove face model and hair at top
			mcHairBack.removeChildAt(1);
			mcHairBack.removeChildAt(1);
			// line up with head
			lineUp(mcHairBack,mcManequin.mcHeadNode);
			// add behind everything (index = 1 because the background is at 0
			addChildAt(mcHairBack,1);
			
			// test: change hair colour
			iColorIndex = CharacterData.getCharacterHairColor(pSerial);
			strColorRecipe = CstDolls.getHairColorRecipe(iColorIndex);
			changeHairColours(strColorRecipe);
			
			// *************************** add Hair
			
			
			
			// apply color recipe to clothes
			strColorRecipe = CstClothes[strClothes].colorRecipe;
			changeClothingColor(strColorRecipe);
			
			
			
		}
		
		public function getIcon():BitmapData{
			// identify extents of icon
			var iIconTopY:int = mcHead.y - 40;
			var iIconBottomY:int = iIconTopY + 60;
			var iIconLeftX:int = mcHead.x - 30;
			var iIconRightX:int = iIconLeftX + 60;
			
			// remove old mask
			removeMask();
			
			// Apply mask
			rectMask = new Shape();
			rectMask.graphics.beginFill(0x000000, 1); //Last arg is the alpha
			rectMask.graphics.drawRoundRect(iIconLeftX, iIconTopY, 60, 60, 10, 10)
			rectMask.graphics.endFill();
			addChild(rectMask);
			this.mask = rectMask;
			
			// remove frame
			removeFrame();
			
			// add Frame on top of everything else
			rectFrame = new Shape();
			rectFrame.graphics.lineStyle(2, 0x00ff00, 1); //Last arg is the alpha
			rectFrame.graphics.beginFill(0x000000, 0); //Last arg is the alpha
			// considering the line thickness, make the fram fit inside the 130 x 130 frame
			rectFrame.graphics.drawRoundRect(iIconLeftX + 1, iIconTopY + 1, 58, 58, 10, 10); 
			rectFrame.graphics.endFill();
			addChild(rectFrame);
			
			// create matrix that will affect the extents of the bitmap data
			var tempMatrix:Matrix = new Matrix();
			tempMatrix.translate(-iIconLeftX,-iIconTopY);
			
			// return bitmap data
			var bmd:BitmapData = new BitmapData(60, 60, true, 0);
			bmd.draw(this,tempMatrix);
			
			return bmd;
		}
		
		public function getPicture():BitmapData{
			// remove old mask
			removeMask();
			
			// Apply mask
			rectMask = new Shape();
			rectMask.graphics.beginFill(0x000000, 1); //Last arg is the alpha
			rectMask.graphics.drawRoundRect(0, 0, 130, 130, 10, 10)
			rectMask.graphics.endFill();
			addChild(rectMask);
			this.mask = rectMask;
			
			// remove frame
			removeFrame();
			
			// add Frame on top of everything else
			rectFrame = new Shape();
			rectFrame.graphics.lineStyle(2, 0x00ff00, 1); //Last arg is the alpha
			rectFrame.graphics.beginFill(0x000000, 0); //Last arg is the alpha
			// considering the line thickness, make the fram fit inside the 130 x 130 frame
			rectFrame.graphics.drawRoundRect(1, 1, 128, 128, 10, 10); 
			rectFrame.graphics.endFill();
			addChild(rectFrame);
			
			// return bitmap data
			var bmd:BitmapData = new BitmapData(130, 130, true, 0);
			bmd.draw(this);
			
			return bmd;
		}
		
		private function changeClothingColor(pRecipe:String){
			var colorFilter:AdjustColor = new AdjustColor();
			var mColorMatrix:ColorMatrixFilter;
			var mMatrix:Array = [];
			//var MC:MovieClip = new MovieClip();
			
			colorFilter.hue = CstClothes[pRecipe].hue;
			colorFilter.saturation = CstClothes[pRecipe].saturation;
			colorFilter.brightness = CstClothes[pRecipe].brightness;
			colorFilter.contrast = CstClothes[pRecipe].contrast;
			
			mMatrix = colorFilter.CalculateFinalFlatArray();
			mColorMatrix = new ColorMatrixFilter(mMatrix);
			
			changeClothingColorElement(mcClothesBody,mColorMatrix);
			changeClothingColorElement(mcClothesUpperArmLeft,mColorMatrix);
			changeClothingColorElement(mcClothesUpperArmRight,mColorMatrix);
			changeClothingColorElement(mcClothesLowerArmRight,mColorMatrix);
			changeClothingColorElement(mcClothesLowerArmLeft,mColorMatrix);
		}
		
		private function changeClothingColorElement(pMc:MovieClip,pMatrix:ColorMatrixFilter){
			// this function applies a color filter to some parts of clothing
			// only if a part is named mcFabric, change color
			
			// unless there is only one child. In this case, do color it
			if(pMc.numChildren == 1){
				pMc.filters = [pMatrix];
				return;
			}
			
			for(var i:int = 0; i< pMc.numChildren; i++){
				if(pMc.getChildAt(i).name == "mcFabric"){
					pMc.getChildAt(i).filters = [pMatrix];
				}
			}
		}
		
		private function changeHairColours(pRecipe:String){
			var colorFilter:AdjustColor = new AdjustColor();
			var mColorMatrix:ColorMatrixFilter;
			var mMatrix:Array = [];
			//var MC:MovieClip = new MovieClip();
			
			colorFilter.hue = CstDolls[pRecipe].hue;
			colorFilter.saturation = CstDolls[pRecipe].saturation;
			colorFilter.brightness = CstDolls[pRecipe].brightness;
			colorFilter.contrast = CstDolls[pRecipe].contrast;
			
			mMatrix = colorFilter.CalculateFinalFlatArray();
			mColorMatrix = new ColorMatrixFilter(mMatrix);
			
			mcHairBack.filters = [mColorMatrix];
			mcHairFront.filters = [mColorMatrix];
			
		}
		
		private function changeSkinColours(pRecipe:String){
			var colorFilter:AdjustColor = new AdjustColor();
			var mColorMatrix:ColorMatrixFilter;
			var mMatrix:Array = [];
			
			colorFilter.hue = CstDolls[pRecipe].hue;
			colorFilter.saturation = CstDolls[pRecipe].saturation;
			colorFilter.brightness = CstDolls[pRecipe].brightness;
			colorFilter.contrast = CstDolls[pRecipe].contrast;
			
			mMatrix = colorFilter.CalculateFinalFlatArray();
			mColorMatrix = new ColorMatrixFilter(mMatrix);
			
			// note: don't affect the nose because, as a child of mcHead, it already receives the filter
			mcHead.filters = [mColorMatrix];
			mcNeck.filters = [mColorMatrix];
			mcBody.filters = [mColorMatrix];
			mcUpperArmLeft.filters = [mColorMatrix];
			mcUpperArmRight.filters = [mColorMatrix];
			mcLowerArmLeft.filters = [mColorMatrix];
			mcLowerArmRight.filters = [mColorMatrix];
		}
		
		private function getPostureFor(pSerial:int):int{
			// finds the frame with the posture of the reference manequin
			
			// find the instrument played
			var strInstrument:String = CharacterData.getCharacterInstrument(pSerial);
			
			// return its position in the list of instruments
			return arInstrumentList.indexOf(strInstrument);
		}
		
		private function lineUp(pBodyPart:MovieClip,pRef:MovieClip){
			pBodyPart.x = pRef.x;
			pBodyPart.y = pRef.y;
			pBodyPart.rotation = pRef.rotation;
			
		}
		
		private function removeClothesAtBack(pMc:MovieClip){
			// go through every child and remove everything called
			// mcBack, mcBackFabric and mcManequin
			
			// process backwards in children, so as to not fuck up the child count when we remove one
			for(var i:int = pMc.numChildren -1; i>= 0; i--){
				if(pMc.getChildAt(i).name == "mcFabricBack" ||
					pMc.getChildAt(i).name == "mcBack" ||
					pMc.getChildAt(i).name == "mcManequin"){
					pMc.removeChildAt(i);
				}
			}
		}
		
		private function removeClothesAtFront(pMc:MovieClip){
			// go through every child and remove everything not called mcBack or mcBackFabric 
			
			// process backwards in children, so as to not fuck up the child count when we remove one
			for(var i:int = pMc.numChildren -1; i>= 0; i--){
				if(pMc.getChildAt(i).name != "mcFabricBack" &&
					pMc.getChildAt(i).name != "mcBack"){
					pMc.removeChildAt(i);
				}
			}
		}
		
		private function removeFrame(){
			// remove the frame if it actually exists
			
			if(rectFrame){
				removeChild(rectFrame);
				rectFrame = null;
			}
		}
		
		private function removeMask(){
			// remove the mask if it actually exists
			
			if(rectMask){
				removeChild(rectMask);
				rectMask = null;
			}
		}
	}
}