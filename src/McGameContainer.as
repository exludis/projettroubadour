package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class McGameContainer extends McPrototype
	{
		// HUD
		private var mcHud:MovieClip;
		
		// Action screen container
		private var mcActionContainer:MovieClip;
		
		// game screens
		private var mcCurrentMainScreen:MovieClip;
		
		// action screen
		private var mcCurrentActionScreen:MovieClip;
		
		// pop-up
		private var mcCurrentPopup:MovieClip;
		
		// temporarily stored values
		private var iStoredSerial:int;
		
		public function McGameContainer(pParent:MovieClip)
		{
			// initialize action manager
			ActionManager.initGameContainer(this);
			
			super(pParent);
			startCityScreen();
			
			// create the layer that contains the action screens
			mcActionContainer = new MovieClip();
			addChild(mcActionContainer);
			
			buildHud();
		}
		
		public function showFirePopupFor(pSerial:int){
			// Store serial
			iStoredSerial = pSerial;
			
			// build pop-up and write proper text **************************
			mcCurrentPopup = new McAssetConfPopup(this);
			
			// get basic message
			var strConfMessage:String = CstStrings.getString("ACTION_FIREDESCR");
			
			// enter name in message
			strConfMessage = strConfMessage.replace("$NAME",CharacterData.getCharacterNamePerSerial(pSerial));
			
			// change his/her based on character gender
			if(CharacterData.checkFemale(pSerial)){
				strConfMessage = strConfMessage.replace("$HISHER","her");
			}else{
				strConfMessage = strConfMessage.replace("$HISHER","his");
			}
			
			//write message in pop-up
			mcCurrentPopup.setText(strConfMessage);
			
			// ************************** build pop-up and write proper text
			
			// add listeners to it
			mcCurrentPopup.addEventListener("yesClicked",fireYesHandler);
			mcCurrentPopup.addEventListener("noClicked",fireNoHandler);
			
			// put it on-screen
			addChild(mcCurrentPopup);
		}
		
		public function showPerformScreen(pTarget:String){
			// build and add staff screen
			mcCurrentActionScreen = new McPerformScreen(this,pTarget);
			mcActionContainer.addChild(mcCurrentActionScreen);
			
			// add event listeners to know when to close this screen
			mcCurrentActionScreen.addEventListener("closeClicked",performCloseHandler);
			mcCurrentActionScreen.addEventListener("doneClicked",performCloseHandler);
		}
		
		public function showRecruitScreen(pTarget:String){
			// retrieve serial number of recruits to show
			var arRecruits:Array = BuildingData.getRecruits(pTarget);
			
			// build recruitment screen
			mcCurrentActionScreen = new McRecruitScreen(this,pTarget);
			mcActionContainer.addChild(mcCurrentActionScreen);
			
			// listen to close button
			mcCurrentActionScreen.addEventListener("closeClicked",closeActionScreenHandler);
		}
		
		public function showStaffScreen(){
			// build and add staff screen
			mcCurrentActionScreen = new McStaffScreen(this);
			mcActionContainer.addChild(mcCurrentActionScreen);
			
			// listen to close button
			mcCurrentActionScreen.addEventListener("closeClicked",closeActionScreenHandler);
		}
		
		public function showUnlockPopup(pIndex:int){
			// create popup
			mcCurrentPopup = new McAssetConfPopup(this);
			
			// Set its graphic
			mcCurrentPopup.updateGraphic(1,CstWagons["emptySpot"].asset);
			
			// Set its values
			var strBaseString:String = CstStrings.getString("ACTION_UNLOCK_DESCR");
			var iCurrentPrice:int = CaravanData.getNextUnlockPrice();
			
			// In the string, replace $INT by the actual price
			strBaseString = strBaseString.replace("$INT",String(iCurrentPrice));
			mcCurrentPopup.setText(strBaseString);
			
			// Add listener
			mcCurrentPopup.addEventListener("yesClicked",unlockYesHandler);
			mcCurrentPopup.addEventListener("noClicked",unlockNoHandler);
			
			// add it, it will place itself
			addChild(mcCurrentPopup);
		}
		
		public function startCaravanScreen(){
			// remove previous screen
			flushCurrentScreen();
			
			// add caravan screen
			mcCurrentMainScreen = new McCaravanScreen(this);
			addChildAt(mcCurrentMainScreen,0); // game screen goes at the bottom of the pile
			
			
		}
		
		public function startCityScreen(){
			// remove previous screen
			flushCurrentScreen();
			
			// show city screen
			mcCurrentMainScreen = new McCityScreen(this,CaravanData.strCurrentLocation);
			addChildAt(mcCurrentMainScreen,0); // game screen goes at the bottom of the pile
			
		}
		
		protected override function addedToStageFunction(){
			// place hud at bottom of screen
			mcHud.y = stage.stageHeight;
			addChild(mcHud);
		}
		
		private function buildHud(){
			mcHud = new McHud(this);
			InterfaceManager.initHud(mcHud);
		}
		
		private function closeActionScreen(){
			mcActionContainer.removeChild(mcCurrentActionScreen);
			mcCurrentActionScreen = null;
		}
		
		private function closeActionScreenHandler(e:Event){
			//trace("closeActionScreenHandler");
			mcCurrentActionScreen.removeEventListener("closeClicked",closeActionScreenHandler);
			closeActionScreen();
		}
		
		private function fireNoHandler(e:Event){
			// remove listeners from pop-up
			mcCurrentPopup.removeEventListener("yesClicked",fireYesHandler);
			mcCurrentPopup.removeEventListener("noClicked",fireNoHandler);
			
			// remove pop-up from screen
			removeChild(mcCurrentPopup);
			mcCurrentPopup = null;
		}
		
		private function fireYesHandler(e:Event){
			// remove listeners from pop-up
			mcCurrentPopup.removeEventListener("yesClicked",fireYesHandler);
			mcCurrentPopup.removeEventListener("noClicked",fireNoHandler);
			
			// Fire
			CaravanData.fireSerial(iStoredSerial);
			
			// remove pop-up from screen
			removeChild(mcCurrentPopup);
			mcCurrentPopup = null;
			
			// dispatch that firing is finished
			dispatchEvent(new Event("firingDone"));
		}
		
		private function flushCurrentScreen(){
			// skip if no main screen exists
			if(!mcCurrentMainScreen){
				return;
			}
			
			// flush main screen
			removeChild(mcCurrentMainScreen);
			mcCurrentMainScreen = null;
		}
		
		private function performCloseHandler(e:Event){
			trace("performCloseHandler");
			// remove event listeners
			mcCurrentActionScreen.removeEventListener("closeClicked",performCloseHandler);
			mcCurrentActionScreen.removeEventListener("doneClicked",performCloseHandler);
			
			// remove action screen
			mcActionContainer.removeChild(mcCurrentActionScreen);
			mcCurrentActionScreen = null;
		}
		
		private function unlockNoHandler(e:Event){
			// remove all listeners on pop-up
			mcCurrentPopup.removeEventListener("yesClicked",unlockYesHandler);
			mcCurrentPopup.removeEventListener("noClicked",unlockNoHandler);
			
			// remove pop-up from screen and carry on
			removeChild(mcCurrentPopup);
			mcCurrentPopup = null;
		}
		
		private function unlockYesHandler(e:Event){
			// remove all listeners on pop-up
			mcCurrentPopup.removeEventListener("yesClicked",unlockYesHandler);
			mcCurrentPopup.removeEventListener("noClicked",unlockNoHandler);
			
			// remove pop-up from screen
			removeChild(mcCurrentPopup);
			mcCurrentPopup = null;
			
			// update spot in the caravan data and lose the amount
			CaravanData.loseMoney(CaravanData.getNextUnlockPrice());
			var iSpotIndex:int = ActionManager.getLastIndex();
			CaravanData.unlockSpotAtIndex(iSpotIndex);
			
			// update the spot on-screen
			mcCurrentMainScreen.updateSpot(iSpotIndex);
		}
	}
}