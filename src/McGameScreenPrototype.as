package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class McGameScreenPrototype extends McPrototype
	{
		
		protected var bHud:Boolean = true; // store whether or not this screen show the hud
		protected var mcContextMenu:MovieClip;
		
		protected var strContextMenuTarget:String; // holds the name of the asset launching the context menu
		protected var iContextMenuType:int; // holds the type of context menu that was lauched
		protected var mcContextMenuSource:MovieClip; // holds the graphic from which the context menu was launched
		private var objContextMenuDef:Object; // holds the definition of the object in the menu
		
		private const iScreenBuffer:int = 20; // minimum distance between context menu and screen edge
		private const iHudBuffer:int = 60; // buffer left for hud at bottom of screen
		
		public function McGameScreenPrototype(pParent:MovieClip)
		{
			super(pParent);
			
		}
		
		public function actionButton(){
			// store index of action button that was clicked
			var iActionIndex = mcContextMenu.iActionButtonIndex;
			
			
			// flush the context menu
			flushContextMenu();
			
			
			// Transmit action to the action manager class
			ActionManager.launchNewAction(iContextMenuType,strContextMenuTarget,0,iActionIndex); // parameter iTargetIndex is irrelevant by default
		}
		
		protected function flushContextMenu(){
			// if there is a context menu on screen, make it go away
			if(mcContextMenu){
				removeChild(mcContextMenu);
				mcContextMenu = null;
			}
		}
		
		protected function showContextMenu(pParent:MovieClip,pType:int,pSource:MovieClip,pDef:String,pIndex:int){
			trace("showContextMenu");
			trace("pParent",pParent,"pType",pType,"pSource",pSource,"pDef",pDef,"pIndex",pIndex);
			
			/*
			Types:
			0: building
			1: wagon
			2: city
			*/
			
			// record arget
			mcContextMenuSource = pSource;
			strContextMenuTarget = pDef;
			iContextMenuType = pType;
			
			// if there is already a context menu on screen, flush it
			flushContextMenu();
			
			var strTypeDef:String;
			var arActions:Array;
			
			mcContextMenu = new McContextMenu(this);
			
			switch(pType){
				case 0:
					// find the object containing the info for this asset
					objContextMenuDef = CstCities[pDef];
					
					// Locate the type definition
					strTypeDef = objContextMenuDef.bldType;
					
					// change graphic
					mcContextMenu.setGraphic(pSource);
					
					// change name
					mcContextMenu.tfName.text = CstStrings.getString(objContextMenuDef.nameTag);
					
					// change type in context menu
					mcContextMenu.tfType.text = CstStrings.getString(CstBuildingDef[strTypeDef].typeTag);
					
					// location actions
					arActions = CstBuildingDef[strTypeDef].actions;
					
					// change description
					mcContextMenu.tfDescription.text = CstStrings.getString(CstBuildingDef[strTypeDef].descrTag);
					break;
				case 1:
					// change graphic
					mcContextMenu.setGraphicWagon(CstWagons[pDef].asset);
					
					// change name
					mcContextMenu.tfName.text = CstStrings.getString(CstWagons[pDef].nameTag);
					
					// change type in context menu
					strTypeDef = CaravanData.getWagonTypeAtIndex(pIndex);
					mcContextMenu.tfType.text = CstStrings.getString("WAGON_GENERIC");
					
					// location actions
					arActions = CstWagons[strTypeDef].actions;
					
					// change description
					mcContextMenu.tfDescription.text = CstStrings.getString(CstWagons[strTypeDef].descrTag);
					
					break;
			}
			
			// build action buttons
			mcContextMenu.buildActionButtons(arActions);
			
			// place the context menu on screen
			placeContextMenuOn(pSource);
			
			// prepare listeners
			mcContextMenu.addEventListener("closeClicked",closeHandler);
			
			// finally, add it
			addChild(mcContextMenu);
		}
		
		private function closeHandler(e:Event){
			flushContextMenu();
		}
		
		private function placeContextMenuOn(pMc:MovieClip){
			// line up with parent object
			//trace("source x",pMc.x,"y",pMc.y);
			mcContextMenu.x = pMc.x;
			mcContextMenu.y = pMc.y;
			
			// now nudge away from edges of the screen
			if(mcContextMenu.x < iScreenBuffer)mcContextMenu.x = iScreenBuffer;
			if(mcContextMenu.x > stage.stageWidth - iScreenBuffer - mcContextMenu.width)mcContextMenu.x = stage.stageWidth - iScreenBuffer - mcContextMenu.width;
			if(mcContextMenu.y < iScreenBuffer)mcContextMenu.y = iScreenBuffer;
			if(mcContextMenu.y > stage.stageHeight - iHudBuffer - mcContextMenu.height){
				// last part in parenthesis: detect if there are graphics going above the background and adjust
				mcContextMenu.y = stage.stageHeight - iHudBuffer - mcContextMenu.height + (Math.max(mcContextMenu.height - mcContextMenu.mcBackground.height,0));
			}
			
		}
	}
}