package 
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class McHomeScreen extends McPrototype{
		
		private var mcCurrentPopup:MovieClip;
		
		public function McHomeScreen(pParent:MovieClip){
			super(pParent);
			
			// new / continue button name changes if there is a saved game under way
			if(SaveManager.checkSaved()){
				mcNewButton.setText(CstStrings.getString("MENU_CONTINUE"));
			}else{
				mcNewButton.setText(CstStrings.getString("MENU_NEWGAME"));
				mcRestartButton.disable();
			}
			
			mcRestartButton.setText(CstStrings.getString("MENU_RESTART"));
			mcCreditsButton.setText(CstStrings.getString("MENU_CREDITS"));
			mcCreditsButton.disable();
			mcLanguageButton.setText(CstStrings.getString("MENU_LANGUAGE"));
			//mcLanguageButton.disable();
		}
		
		protected override function addedToStageFunction(){
			//trace("home screen added");
			mcNewButton.addEventListener("buttonClicked",newClicked);
			mcLanguageButton.addEventListener("buttonClicked",languageClicked);
			mcRestartButton.addEventListener("buttonClicked",restartClicked);
		}
		
		protected override function removedFromStageFunction(){
			mcNewButton.removeEventListener("buttonClicked",newClicked);
			mcLanguageButton.removeEventListener("buttonClicked",languageClicked);
		}
		
		private function restartClicked(e:Event){
			// show a confirmation pop-up to make sure the data will not be lost by mistake
			mcCurrentPopup = new McConfPopup(this);
			mcCurrentPopup.addEventListener("yesClicked",restartYesHandler);
			mcCurrentPopup.addEventListener("noClicked",restartNoHandler);
			addChild(mcCurrentPopup);
			
			
		}
		
		private function restartNoHandler(e:Event){
			// remove listeners on pop-up
			mcCurrentPopup.removeEventListener("yesClicked",restartYesHandler);
			mcCurrentPopup.removeEventListener("noClicked",restartNoHandler);
			
			// remove pop-up from screen
			removeChild(mcCurrentPopup);
			mcCurrentPopup = null;
			
			// reset restart button
			mcRestartButton.enable();
		}
		
		private function restartYesHandler(e:Event){
			// remove listeners on pop-up
			mcCurrentPopup.removeEventListener("yesClicked",restartYesHandler);
			mcCurrentPopup.removeEventListener("noClicked",restartNoHandler);
			
			// delete saved data
			SaveManager.deleteSavedData();
			
			// rename continue button to new game button
			mcNewButton.setText(CstStrings.getString("MENU_NEWGAME"));
			
			// remove pop-up from screen
			removeChild(mcCurrentPopup);
			mcCurrentPopup = null;
			
			// disable restart button
			mcRestartButton.disable();
		}
		
		private function languageClicked(e:Event){
			dispatchEvent(new Event("languageClicked"));
		}
		
		private function newClicked(e:Event){
			//trace("click event captured");
			dispatchEvent(new Event("newGame"));
		}
	}
}