package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class McHud extends McPrototype
	{
		public function McHud(pParent:MovieClip)
		{
			super(pParent);
			
			//update text in next day button
			mcNextDayButton.setText(CstStrings.getString("HUD_NEXTDAY"));
			
			// Start listeners on buttons
			startButtonListeners();
			
			// update money monitor
			updateStats();
		}
		
		public function startButtonListeners(){
			mcMapButton.addEventListener("buttonClicked",mapButtonClickHandler);
			mcNextDayButton.addEventListener("buttonClicked",dayButtonClickHandler);
		}
		
		public function updateStats(){
			tfMoneyMonitor.text = String(CaravanData.iMoney);
		}
		
		private function dayButtonClickHandler(e:Event){
			trace("next day");
		}
		
		private function mapButtonClickHandler(e:Event){
			trace("map");
		}
	}
}