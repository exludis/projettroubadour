package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class McLanguageScreen extends McPrototype
	{
		public function McLanguageScreen(pParent:MovieClip)
		{
			super(pParent);
			
			// change prompt text
			tfLanguagePrompt.text = CstStrings.getString("MENU_LANGUAGE_PROMPT");
			
			// change button text
			mcLanguageButton1.setText(CstStrings.getLanguage(0));
			mcLanguageButton2.setText(CstStrings.getLanguage(1));
		}
		
		protected override function addedToStageFunction(){
			// start listeners on buttons
			mcLanguageButton1.addEventListener("buttonClicked",btn1ClickHandler);
			mcLanguageButton2.addEventListener("buttonClicked",btn2ClickHandler);
		}
		
		protected override function removedFromStageFunction(){
			// start listeners on buttons
			mcLanguageButton1.removeEventListener("buttonClicked",btn1ClickHandler);
			mcLanguageButton2.removeEventListener("buttonClicked",btn2ClickHandler);
		}
		
		private function btn1ClickHandler(e:Event){
			Settings.setLanguage(0);
			dispatchEvent(new Event("languagePicked"));
		}
		
		private function btn2ClickHandler(e:Event){
			Settings.setLanguage(1);
			dispatchEvent(new Event("languagePicked"));
		}
	}
}