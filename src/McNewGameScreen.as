package
{
	import flash.display.MovieClip;
	import flash.events.Event;

	public class McNewGameScreen extends McPrototype
	{
		public function McNewGameScreen(pParent:MovieClip)
		{
			super(pParent);
			
			// make the prompt invisible to mous
			tfPrompt.mouseEnabled = false;
			
			// set strings
			tfExplanation.text = CstStrings.getString("MENU_NEWGAMEEXPL");
			tfPrompt.text = CstStrings.getString("MENU_ENTERNAME");
			mcPlayButton.setText(CstStrings.getString("MENU_PLAYBTN"));
			
			// prepare the text input box
			updateEmptyTextBox();
			tfNameInput.addEventListener(Event.CHANGE,inputTextHandler);
			
			// listen to play button
			mcPlayButton.addEventListener("buttonClicked",playButtonHandler);
		}
		
		public function getName():String{
			return tfNameInput.text;
		}
		
		public function getTuto():Boolean{
			return mcTutoCheckbox.getCheck();
		}
		
		private function inputTextHandler(e:Event){
			//trace("text input");
			// if the textbox is empty, show the gray "enter name here" text and enable play button
			if(tfNameInput.text == ""){
				updateEmptyTextBox();
			
			// otherwise, hide the gray text and enable play button
			}else{
				tfPrompt.alpha = 0;
				mcPlayButton.enable();
			}
		}
		
		private function playButtonHandler(e:Event){
			mcPlayButton.removeEventListener("buttonClicked",playButtonHandler);
			dispatchEvent(new Event("playClicked"));
		}
		
		protected override function removedFromStageFunction(){
			tfNameInput.removeEventListener(Event.CHANGE,inputTextHandler);
		}
		
		private function updateEmptyTextBox(){
			// disable play button until the troup has a name
			mcPlayButton.disable();
			tfPrompt.alpha = 1.0;
		}
	}
}