package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	public class McPerformScreen extends McActionScreenPrototype
	{
		private var bShowingSelect:Boolean = false;
		
		private var arPerformerSlots:Array;
		private var arPerformerCards:Array; // holds the cards in the performer slot
		private var arRemoveButtons:Array; // holds the (x) buttons on performer cards
		
		private var mcCurrentMousePerformer:MovieClip; // holds the performer currently having a mouse over
		
		private var iCurrentCharSerial:int; // serial of selected character
		
		private var mcDraggedCard:MovieClip;
		
		private var mcRemoveButton:MovieClip;
		
		public function McPerformScreen(pParent:MovieClip,pTarget:String)
		{
			super(pParent);
			
			// record target building
			strTarget = pTarget;
			
			// populate character list in select interface
			var arArtists:Array = CaravanData.getStaff();
			//trace("found artists",arArtists);
			mcSelect.mcCharList.buildCharList(arArtists);
			
			// hide select interface by default
			removeChild(mcSelect);
			
			// record close button
			mcCloseButton = mcClose;
			
			// record background
			mcProtoBackground = mcBackground;
			
			// update title and target text, and pay legend
			tfTitle.text = CstStrings.getString("ACTION_PERFORM");
			tfTarget.text = CstStrings.getString(CstCities[pTarget].nameTag);
			tfPayLegend.text = CstStrings.getString("ACTION_PAY");
			
			// update button text
			mcDoneButton.setText(CstStrings.getString("GEN_DONE"));
			mcSelectButton.setText(CstStrings.getString("ACTION_SELECTPERFORMERS"));
			
			// write info *************************************************
			
			// fill out owner data
			mcInfo.tfOwnerLegend.text = CstStrings.getString("ACTION_OWNER");
			mcInfo.tfOwner.text = CstCities[strTarget].ownerName;
			mcInfo.tfLikedLegend.text = CstStrings.getString("ACTION_LIKED");
			mcInfo.tfDislikedLegend.text = CstStrings.getString("ACTION_DISLIKED");
			
			// write liked and disliked genres for owner
			mcInfo.tfLiked.text = "";
			var strGenreNameTag:String;
			for each(var strGenre:String in CstCities[strTarget].ownerLikes){
				// skip line if this is not the first one
				if(mcInfo.tfLiked.text != "")mcInfo.tfLiked.appendText("\n");
				strGenreNameTag = CstGenres[strGenre].nameTag;
				mcInfo.tfLiked.appendText(CstStrings.getString(strGenreNameTag));
			}
			
			mcInfo.tfDisliked.text = "";
			for each(strGenre in CstCities[strTarget].ownerDislikes){
				// skip line if this is not the first one
				if(mcInfo.tfDisliked.text != "")mcInfo.tfDisliked.appendText("\n");
				strGenreNameTag = CstGenres[strGenre].nameTag;
				mcInfo.tfDisliked.appendText(CstStrings.getString(strGenreNameTag));
			}
			
			// get today's patrons
			var strPatrons:String = BuildingData.getPatrons(strTarget);
			
			// fill out patrons data
			mcInfo.tfPatronsLegend.text = CstStrings.getString("ACTION_PATRONS");
			var strPatronsNameTag:String = CstPatrons[strPatrons].nameTag;
			mcInfo.tfPatrons.text = CstStrings.getString(strPatronsNameTag);
			mcInfo.tfPatronsLikedLegend.text = CstStrings.getString("ACTION_LIKED");
			mcInfo.tfPatronsDislikedLegend.text = CstStrings.getString("ACTION_DISLIKED");
			
			// write liked and dislikes genres for patrons
			mcInfo.tfPatronsLiked.text = "";
			for each(strGenre in CstPatrons[strPatrons].likes){
				// skip line if this is not the first one
				if(mcInfo.tfPatronsLiked.text != "")mcInfo.tfPatronsLiked.appendText("\n");
				strGenreNameTag = CstGenres[strGenre].nameTag;
				mcInfo.tfPatronsLiked.appendText(CstStrings.getString(strGenreNameTag));
			}
			
			mcInfo.tfPatronsDisliked.text = "";
			for each(strGenre in CstPatrons[strPatrons].dislikes){
				// skip line if this is not the first one
				if(mcInfo.tfPatronsDisliked.text != "")mcInfo.tfPatronsDisliked.appendText("\n");
				strGenreNameTag = CstGenres[strGenre].nameTag;
				mcInfo.tfPatronsDisliked.appendText(CstStrings.getString(strGenreNameTag));
			}
			
			// ************************************************* write info
			
			// update performer slots *************************************
			arPerformerSlots =  [mcSlot1,mcSlot2,mcSlot3,mcSlot4];
			
			// disable extra slots
			for(var i:int = 0; i < arPerformerSlots.length; i++){
				if(i < CstCities[pTarget].maxArtists){
					arPerformerSlots[i].gotoAndStop("enabled");
					arPerformerSlots[i].mcPickButton.setText(CstStrings.getString("ACTION_PICK")); // update "pick" button
					arPerformerSlots[i].mcPickButton.iStoredInt = i; // store the card index inside the button for later retrieval
				}else{
					arPerformerSlots[i].gotoAndStop("disabled");
				}
			}
			
			// remove extra slots from array
			arPerformerSlots.splice(CstCities[pTarget].maxArtists,arPerformerSlots.length -1);
			
			// ************************************* update performer slots
			
			arPerformerCards = new Array(4); // init performer cards array
			arRemoveButtons = new Array(4);
			
			// populate performer slots *************************************
			// get list of assigned artists
			var arTempParticipants:Array = AssignmentData.getParticipantsAt(strTarget);
			
			// spawn cards for each not empty index in the array
			for(i = 0; i < arTempParticipants.length; i++){
				// if this slot is not empty, spawn its content
				if(arTempParticipants[i] != -1 && arTempParticipants[i] != undefined){
					spawnCard(arTempParticipants[i],i);
				}
			}
			
			// ************************************* populate performer slots
			
			pickButtonsDisable(); // disable pick buttons until a character is selected
			
		}
		
		protected override function addedToStageFunction2(){
			// add listener on close and done buttons
			mcCloseButton.addEventListener("buttonClicked",clickCloseHandler);
			mcDoneButton.addEventListener("buttonClicked",clickDoneHandler);
			mcSelect.mcCharList.addEventListener("cardChange",cardClickHandler);
			
			// add listener to select button
			mcSelectButton.addEventListener("buttonClicked",clickSelectHandler);
			
			// add listeners to pick buttons
			for(var i:int = 0; i < arPerformerSlots.length; i++){
				arPerformerSlots[i].mcPickButton.addEventListener("buttonClicked",pickHandler);
			}
			
			// add listeners to performer cards
			resetPerformerListeners();
			
			// update character list
			mcSelect.mcCharList.disableOnAssignment();
			
			// updat price monitor
			updatePriceMonitor();
		}
		
		protected override function removedFromStageFunction(){
			// remove event listeners for done and close buttons
			mcCloseButton.removeEventListener("buttonClicked",clickCloseHandler);
			mcDoneButton.removeEventListener("doneClicked",clickDoneHandler);
			mcSelect.mcCharList.removeEventListener("cardChange",cardClickHandler);
			
			// remove listener to select button
			mcSelectButton.removeEventListener("buttonClicked",clickSelectHandler);
			
			// remove listeners from pick buttons
			for(var i:int = 0; i < arPerformerSlots.length; i++){
				arPerformerSlots[i].mcPickButton.removeEventListener("buttonClicked",pickHandler);
			}
			
			// remove listeners from performer cards if they are there
			//trace("cleaning performer cards");
			for each(var mc:MovieClip in arPerformerCards){
				if(mc != null){
					if(mc.hasEventListener("buttonClicked"))mc.removeEventListener("buttonClicked",performerClickHandler);
				}
			}
			
			// remove listeners from remove buttons
			//trace("cleaning remove buttons");
			for each(mc in arRemoveButtons){
				if(mc != null){
					if(mc.hasEventListener("buttonClicked"))mc.removeEventListener("buttonClicked",removeHandler);
				}
			}
		}
		
		private function cardClickHandler(e:Event){
			// record serial
			iCurrentCharSerial = mcSelect.mcCharList.getCurrentSerial();
			
			// update character inspector
			mcSelect.mcCharInspector.inspectSerial(iCurrentCharSerial);
			
			// enable pick buttons
			pickButtonsEnable();
			
			// update character list
			mcSelect.mcCharList.disableOnAssignment();
			
			// update listeners on performer cards
			updatePerformerCards();
		}
		
		private function clickCloseHandler(e:Event){
			// dispach event that it's time to close
			dispatchEvent(new Event("closeClicked"));
		}
		
		private function clickDoneHandler(e:Event){
			// dispach event that it's time to close
			//trace("clickDoneHandler");
			dispatchEvent(new Event("doneClicked"));
		}
		
		private function clickSelectHandler(e:Event){
			// invert tracking boolean
			bShowingSelect = !bShowingSelect;
			
			// if info is on screen, change it to selection, and vice-versa
			if(bShowingSelect == false){
				removeChild(mcSelect);
				addChild(mcInfo);
				
				// update text on select button
				mcSelectButton.setText(CstStrings.getString("ACTION_SELECTPERFORMERS"));
				
			}else{
				removeChild(mcInfo);
				addChild(mcSelect);
				
				mcSelect.mcCharList.forceListenersOn();
				
				// update text on select button
				mcSelectButton.setText(CstStrings.getString("ACTION_PERFORMINFO"));
			}
			
			// re-enable the select button
			mcSelectButton.enable();
		}
		
		private function performerClickHandler(e:Event){
			//trace("performerClickHandler");
			
			// record serial, make it absolute because a negative serial can be used to differentiate artists
			iCurrentCharSerial = Math.abs(e.currentTarget.getSerial());
			// update character inspector
			mcSelect.mcCharInspector.inspectSerial(iCurrentCharSerial);
			
			// disable pick buttons
			pickButtonsDisable();
			
			// update character list
			mcSelect.mcCharList.disableOnAssignment();
			
			// update performer cards
			updatePerformerCards();
		}
		
		private function pickButtonsDisable(){
			for(var i:int = 0; i < arPerformerSlots.length; i++){
				arPerformerSlots[i].mcPickButton.disable();
			}
		}
		
		private function pickButtonsEnable(){
			for(var i:int = 0; i < arPerformerSlots.length; i++){
				arPerformerSlots[i].mcPickButton.enable();
			}
		}
		
		private function pickHandler(e:Event){
			// retrieve number of the button
			var iSlotIndex:int = e.currentTarget.iStoredInt;
			
			// spawn the card in the proper slot
			spawnCard(iCurrentCharSerial,iSlotIndex);
			
			// update performer data
			AssignmentData.addArtistToAssignment(iCurrentCharSerial,strTarget,"perform",iSlotIndex);
			
			// gray out the original artist card
			mcSelect.mcCharList.disableCardWithSerial(iCurrentCharSerial);
			
			// disable "pick" buttons
			pickButtonsDisable();
			
			// update character list
			mcSelect.mcCharList.disableOnAssignment();
			
			// highlight the card in the performance slot because, after all, we're still inspecting this character
			arPerformerCards[iSlotIndex].forceClicked();
			
			// update listeners on performer cards
			updatePerformerCards();
			
			// reset listeners to performer cards
			resetPerformerListeners();
			
			// updat price monitor
			updatePriceMonitor();
		}
		
		private function removeHandler(e:Event){
			//trace("removeHandler");
			
			// retrieve slot number
			var iTempSlot:int = e.currentTarget.iStoredInt;
			
			// remove artist from assignments
			AssignmentData.freeSlot(strTarget,"perform",iTempSlot);
			
			// delete character card and remove button
			removeChild(arPerformerCards[iTempSlot]);
			arPerformerCards[iTempSlot] = null;
			removeChild(arRemoveButtons[iTempSlot]);
			arRemoveButtons[iTempSlot] = null;
			
			// update list
			mcSelect.mcCharList.disableOnAssignment();
			
			// updat price monitor
			updatePriceMonitor();
			
			// empty character inspector
			mcSelect.mcCharInspector.makeEmpty();
		}
		
		private function resetPerformerListeners(){
			// check each performer card one by one. Add listeners if it's missing them
			for each(var mc:MovieClip in arPerformerCards){
				
				// skip if this is null
				if(mc == null){
					break;
				}
				
				if(!mc.hasEventListener("buttonClicked")){
					mc.addEventListener("buttonClicked",performerClickHandler);
					//mc.addEventListener("buttonOver",performerOverHandler);
				}
			}
		}
		
		private function spawnCard(pSerial:int,pSlot:int){
			//trace("spawn card fo serial",pSerial,"in slot",pSlot);
			var mcTempCard:MovieClip = new McCharacterCard(this);
			mcTempCard.showCharacter(pSerial);
			mcTempCard.x = arPerformerSlots[pSlot].x;
			mcTempCard.y = arPerformerSlots[pSlot].y;
			addChild(mcTempCard);
			arPerformerCards[pSlot] = mcTempCard;
			
			// spawn the remove button
			var mcTempRemove:MovieClip = new McCloseButtonSmall();
			mcTempRemove.x = arPerformerSlots[pSlot].x;
			mcTempRemove.y = arPerformerSlots[pSlot].y;
			mcTempRemove.iStoredInt = pSlot; // record slot number in the remove button
			addChild(mcTempRemove);
			arRemoveButtons[pSlot] = mcTempRemove;
			mcTempRemove.addEventListener("buttonClicked",removeHandler);
		}
		
		private function updatePerformerCards(){
			//trace("updatePerformerCards");
			for each(var mc:MovieClip in arPerformerCards){
				
				// skip if null
				if(mc == null){
					break;
				}
				
				// enable card, unless this character is currently inspected
				if(mc.getSerial() == iCurrentCharSerial){
					mc.forceClicked();
				}else{
					mc.enable();
				}
			}
		}
		
		private function updatePriceMonitor(){
			//trace("updatePriceMonitor");
			
			// get price from AssignmentData
			var iPrice:int = AssignmentData.getPrice(strTarget,"perform");
			tfPayMonitor.text = String(iPrice);
		}
		
	}
}