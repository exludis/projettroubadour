package
{
	import flash.display.MovieClip;
	
	public class McPopupPrototype extends McPrototype
	{
		private var mcBackdrop:MovieClip;
		
		public function McPopupPrototype(pParent:MovieClip)
		{
			super(pParent);
		}
		
		protected override function addedToStageFunction(){
			addedToStageFunction2();
			
			// place self in the middle of the screen
			this.x = stage.stageWidth / 2 - this.width / 2;
			this.y = stage.stageHeight / 2 - 60 - this.height / 2; // 60 is a buffer for the hud at the bottom of the screen
			
			// add backdrop behind pop-up
			addBackdrop();
			
		}
		
		protected function addedToStageFunction2(){
			// this is a dummy function. It exists so that children classes can add stuff to addedToStageFunction;
		}
		
		private function addBackdrop(){
			mcBackdrop = new McBackdrop();
			mcBackdrop.x = -this.x;
			mcBackdrop.y = -this.y;
			addChildAt(mcBackdrop,0);
		}
	}
}