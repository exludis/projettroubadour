package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	public class McPreloader extends MovieClip {
		
		private var mcMyParent:MovieClip;
		private var timLoadedTimer:Timer;
		private const iLoadedTime:int = 250; // number of ms before the loader vanishes after it reaches 100%
		
		public function McPreloader(){
		}
		
		public function initParent(pMc){
			mcMyParent = pMc;
		}
		
		public function startMonitoring(){
			addEventListener(Event.ENTER_FRAME,loadingFrameHandler);
		}
		
		private function loadingFrameHandler(e:Event){
			// this function finds the percent of movieclip loaded and displays it
			var iPercentLoaded:int = mcMyParent.getPercentLoaded();
			trace("iPercentLoaded",iPercentLoaded);
			tfLoadingMonitor.text = String(iPercentLoaded+" %");
			
			// if we're at 100%, stop monitoring and start a short timer
			if(iPercentLoaded == 100){
				removeEventListener(Event.ENTER_FRAME,loadingFrameHandler);
				timLoadedTimer = new Timer(1,iLoadedTime);
				timLoadedTimer.addEventListener(TimerEvent.TIMER,timerCompleteHandler);
				timLoadedTimer.start();
			}
		}
		
		private function timerCompleteHandler(e:TimerEvent){
			// clean up after timer
			timLoadedTimer.removeEventListener(TimerEvent.TIMER,timerCompleteHandler);
			timLoadedTimer = null;
			
			// dispatch event to say we're done here
			dispatchEvent(new Event("loadingComplete"));
		}
	}
}