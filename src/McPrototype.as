package 
{
	import flash.display.MovieClip;
	import flash.events.Event;

	public class McPrototype extends MovieClip {
		
		protected var mcMyParent:MovieClip;
		
		public function McPrototype(pParent:MovieClip){
			//stop movieclip
			stop();
			
			mcMyParent = pParent;
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
		}
		
		protected function addedToStageFunction(){
			
		}
		
		protected function addedToStageHandler(e:Event){
			removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
			addedToStageFunction();
			addEventListener(Event.REMOVED_FROM_STAGE,removedFromStageHandler);
		}
		
		protected function removedFromStageFunction(){
			
		}
		
		protected function removedFromStageHandler(e:Event){
			removeEventListener(Event.REMOVED_FROM_STAGE,removedFromStageHandler);
			removedFromStageFunction();
		}
	}
}