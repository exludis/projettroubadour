package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class McRecruitScreen extends McActionScreenPrototype
	{
		private var iCurrentCharSerial:int; // serial of character currently being inspected
		
		public function McRecruitScreen(pParent:MovieClip,pTarget:String)
		{
			super(pParent);
			// record close button
			mcCloseButton = mcClose;
			
			// record background
			mcProtoBackground = mcBackground;
			
			// record target data
			strTarget = pTarget;
			
			// Write title and target
			tfTitle.text = CstStrings.getString("ACTION_RECRUIT");
			tfTarget.text = CstStrings.getString(CstCities[strTarget].nameTag);
			
			// Change text in "hire" button
			mcHireButton.setText(CstStrings.getString("ACTION_HIRE"));
			
			// Build character list
			var arRecruits:Array = BuildingData.getRecruits(strTarget);
			mcCharList.setEmptyMessage(CstStrings.getString("ACTION_RECRUIT_NONE"));
			mcCharList.buildCharList(arRecruits);
			
			// reset character serial
			iCurrentCharSerial = -1;
			
			// update info on screen
			updateMessageAndButton();
		}
		
		protected override function addedToStageFunction2(){
			mcCharList.addEventListener("cardChange",cardClickHandler);
			mcHireButton.addEventListener("buttonClicked",hireClickHandler);
		}
		
		protected override function removedFromStageFunction(){
			mcCharList.removeEventListener("cardChange",cardClickHandler);
			mcHireButton.removeEventListener("buttonClicked",hireClickHandler);
		}
		
		private function cardClickHandler(e:Event){
			// record serial
			iCurrentCharSerial = mcCharList.getCurrentSerial();
			
			// update character inspector
			mcCharInspector.inspectSerial(iCurrentCharSerial);
			
			// update info on screen
			updateMessageAndButton();
		}
		
		private function hireClickHandler(e:Event){
			//trace("hireClickHandler");
			
			// move this character's serial from the source to the caravan
			BuildingData.removeRecruit(strTarget,iCurrentCharSerial);
			CaravanData.hireArtist(iCurrentCharSerial);
			
			// remove the card from the list
			mcCharList.removeCharSerial(iCurrentCharSerial);
			
			// reset character serial
			iCurrentCharSerial = -1;
			
			// empty the character inspector
			mcCharInspector.makeEmpty();
			
			// update info on screen
			updateMessageAndButton();
		}
		
		private function updateMessageAndButton(){
			
			// no character on screen, disable.
			if(iCurrentCharSerial == -1){
				tfError.text = "";
				mcHireButton.disable();
				return;
			}
			
			// not enough money, disable
			if(CaravanData.iMoney < CharacterData.getCostPerSerial(iCurrentCharSerial)){
				tfError.text = CstStrings.getString("ACTION_RECRUIT_NOFUNDS");
				mcHireButton.disable();
				return;
			}
			
			// not enough place, disable
			if(CaravanData.getFreeSpots() == 0){
				tfError.text = CstStrings.getString("ACTION_RECRUIT_NOROOM");
				mcHireButton.disable();
				return;
			}
			
			// all checks passed, enable
			tfError.text = "";
			mcHireButton.enable();
			
		}
	}
}