package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class McStaffScreen extends McActionScreenPrototype
	{
		private var iCurrentCharSerial:int; // serial of character currently being inspected
		
		public function McStaffScreen(pParent:MovieClip)
		{
			super(pParent);
			// record close button
			mcCloseButton = mcClose;
			
			// record background
			mcProtoBackground = mcBackground;
			
			// Write title and target
			tfTitle.text = CstStrings.getString("ACTION_RECRUIT");
			
			// Change text in buttons
			mcRenewButton.setText(CstStrings.getString("ACTION_RENEW"));
			mcFireButton.setText(CstStrings.getString("ACTION_FIRE"));
			mcLodgingButton.setText(CstStrings.getString("ACTION_CHANGELODGING"));
			mcLodgingButton.disable();
			mcEquipmentButton.setText(CstStrings.getString("ACTION_CHANGEEQUIP"));
			mcEquipmentButton.disable();
			
			// Build character list
			var arStaff:Array = CaravanData.getStaff();
			mcCharList.setEmptyMessage(CstStrings.getString("ACTION_NOSTAFF"));
			mcCharList.buildCharList(arStaff);
			
			// reset character serial
			iCurrentCharSerial = -1;
		}
		
		protected override function addedToStageFunction2(){
			mcCharList.addEventListener("cardChange",cardClickHandler);
			mcRenewButton.addEventListener("buttonClicked",renewHandler);
			mcFireButton.addEventListener("buttonClicked",fireHandler);
		}
		
		protected override function removedFromStageFunction(){
			mcCharList.removeEventListener("cardChange",cardClickHandler);
			mcRenewButton.removeEventListener("buttonClicked",renewHandler);
			mcFireButton.removeEventListener("buttonClicked",fireHandler);
		}
		
		private function cardClickHandler(e:Event){
			// record serial
			iCurrentCharSerial = mcCharList.getCurrentSerial();
			
			// update character inspector
			mcCharInspector.inspectSerial(iCurrentCharSerial);
		}
		
		private function fireDoneHandler(e:Event){
			// update char list and inspector
			mcCharList.removeCharSerial(iCurrentCharSerial);
			mcCharInspector.makeEmpty();
		}
		
		private function fireHandler(e:Event){
			// get serial of selected character
			iCurrentCharSerial = mcCharList.getCurrentSerial();
			
			// re-enable button
			mcFireButton.enable();
			
			// add listener on game container so that we know when the pop-up sequence is over
			mcMyParent.addEventListener("firingDone",fireDoneHandler);
			
			// start firing interface in game container
			mcMyParent.showFirePopupFor(iCurrentCharSerial);
		}
		
		private function renewHandler(e:Event){
			// get serial of selected character
			iCurrentCharSerial = mcCharList.getCurrentSerial();
			
			// renew contract
			CaravanData.renewContractFor(iCurrentCharSerial);
			
			// update character inspector
			mcCharInspector.update();
			
			// reset button
			mcRenewButton.enable();
		}
		
	}
}