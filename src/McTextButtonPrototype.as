package   
{
	import flash.text.TextField;

	public class McTextButtonPrototype extends McButtonPrototype{
		
		protected var tfBaseTextField:TextField;
		
		public function McTextButtonPrototype()
		{
			super();
		}
		
		public function setText(pText:String){
			tfBaseTextField.text = pText;
		}
	}
}