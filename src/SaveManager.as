package
{
	import flash.net.SharedObject;

	public class SaveManager
	{
		private static var saveDataObject:SharedObject;
		
		public static function checkSaved():Boolean{
			if(saveDataObject.data.strCaravanName == null){
				return false;
			}
			
			return true;
		}
		
		public static function deleteSavedData(){
			saveDataObject.data.strCaravanName = null;
			saveDataObject.data.arBuildings = null;
			saveDataObject.data.arCharacters = null;
			saveDataObject.data.iMoney = null;
			saveDataObject.data.strCaravanName = null;
			saveDataObject.data.arArtists = null;
			saveDataObject.data.arAssignments = null;
		}
		
		public static function getHiredArtists():Array{
			return saveDataObject.data.arArtists;
		}
		
		public static function getAssignments():Array{
			return saveDataObject.data.arAssignments;
		}
		
		public static function getBuildings():Array{
			return saveDataObject.data.arBuildings;
		}
		
		public static function getCharacters():Array{
			return saveDataObject.data.arCharacters;
		}
		
		public static function getLanguage():int{
			return saveDataObject.data.iLang;
		}
		
		public static function getLocation():String{
			return saveDataObject.data.strCurrentLocation;
		}
		
		public static function getMoney():int{
			return saveDataObject.data.iMoney;
		}
		
		public static function getName():String{
			return saveDataObject.data.strCaravanName;
		}
		
		public static function getWagons():Array{
			return saveDataObject.data.arWagons;
		}
		
		public static function init(){
			saveDataObject = SharedObject.getLocal("troubadours");
		}
		
		/*public static function initNewSave(){
			
		}*/
		
		public static function saveArtists(pArray:Array){
			saveDataObject.data.arArtists = pArray;
			saveDataObject.flush();
		}
		
		public static function saveAssignments(pArray:Array){
			saveDataObject.data.arAssignments = pArray;
			saveDataObject.flush();
		}
		
		public static function saveBuildings(pArray:Array){
			saveDataObject.data.arBuildings = pArray;
			saveDataObject.flush();
		}
		
		public static function saveCharacters(pArray:Array){
			saveDataObject.data.arCharacters = pArray;
			saveDataObject.flush();
		}
		
		public static function saveLanguage(pLang:int){
			saveDataObject.data.iLang = pLang;
			saveDataObject.flush();
		}
		
		public static function saveLocation(pCity:String){
			saveDataObject.data.strCurrentLocation = pCity;
			saveDataObject.flush();
		}
		
		public static function saveMoney(pMoney:int){
			saveDataObject.data.iMoney = pMoney;
			saveDataObject.flush();
		}
		
		public static function saveName(pName:String){
			saveDataObject.data.strCaravanName = pName;
			saveDataObject.flush();
		}
		
		public static function saveWagons(pArray:Array){
			saveDataObject.data.arWagons = pArray;
			saveDataObject.flush();
		}
	}
}