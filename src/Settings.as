package 
{
	public class Settings
	{
		public static const strStartingCity:String = "testVillage";
		
		
		public static var iCurentLanguage:int = 0;
		public static var bTuto:Boolean = false;
		
		public static function retrieveSavedData(){
			iCurentLanguage = SaveManager.getLanguage();
		}
		
		public static function setLanguage(pLang:int){
			iCurentLanguage = pLang;
			SaveManager.saveLanguage(iCurentLanguage);
		}
		
	}
	
}