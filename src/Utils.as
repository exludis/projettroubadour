package
{
	public class Utils
	{
		
		public static function getRandomIndices(pPicks:int,pLength:int):Array{
			trace("getRandomIndices",pPicks,"out of",pLength);
			var arTempArray =  new Array();
			
			// fill array with numbers from 0 to X
			for(var i:int = 0 ; i < pLength; i++){
				arTempArray.push(i);
			}
			trace("new array",arTempArray);
			//remove non picks
			var iPicksToRemove:int = pLength - pPicks;
			var iRandomRemove:int;
			for(i = 0 ; i < iPicksToRemove; i++){
				iRandomRemove = Math.random() * arTempArray.length;
				arTempArray.splice(iRandomRemove,1);
			}
			
			return arTempArray;
		}
		
		public static function getRandomIntBetween(pMin:int,pMax:int):int{
			return Math.random() * (pMax - pMin + 1) + pMin;
		}
		
		public static function getWeightedRandom(pMin:int,pAvg:int,pMax:int):int{
			// This function generates a random number between min and max.
			// This number is adjusted so that the average pick corresponds to the average number
			var iFinalPick:int;
			
			// Generate a number between -1 and +1
			var numVarFromAvg:Number = Math.random() * 2 - 1;
			
			// If number is below zero, pick between min and avg
			if(numVarFromAvg < 0){
				iFinalPick = Math.round(pAvg + numVarFromAvg * (pAvg - pMin));
				
			// If number is above zero, pick between avg and max
			}else{
				iFinalPick = Math.round(pAvg + numVarFromAvg * (pMax - pAvg));
			}
			
			return iFinalPick;
		}
		
		public static function invertStat(pInt:int):int{
			// inverts a stat
			// example: 75 becomes 25, or vice versa
			return 100 - pInt;
		}
		
		public static function pickAtRandom(pArray:Array):int{
			return Math.random() * pArray.length;
		}
		
		public static function setMinMax(pOriginal:int,pMin:int,pMax:int):int{
			pOriginal = Math.max(pOriginal,pMin);
			pOriginal = Math.min(pOriginal,pMax);
			return pOriginal;
		}
	}
}