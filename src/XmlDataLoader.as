package
{
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	public class XmlDataLoader
	{
		
		private static var xmlData:XML;
		private static var xmlLoader:URLLoader;
		
		/*public function XmlDataLoader()
		{
		}*/
		
		public static function loadXmlFile(pFileName:String){
			// load xml file
			xmlLoader = new URLLoader();
			xmlLoader.addEventListener(Event.COMPLETE, doneLoading);
			xmlLoader.load(new URLRequest(pFileName+".xml"));
		}
		
		private static function doneLoading(e:Event){
			xmlData = new XML(xmlLoader.data);
			trace("Loaded XML");
			trace(xmlData);
		}
		
	}
}